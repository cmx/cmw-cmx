/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <iostream>
#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/Log.h>
#include <getopt.h>
#include <csignal>

using namespace cmw::cmx;

static void cmxadm_help()
{
    std::cout << ("Usage:\n"
            "  -r      Register a test component\n"
            "  -u      undo -r or no-op\n"
            "  -p      Register process (Process::init)\n"
            "  -c      Registry::cleanup()\n"
            "  -l      List all components\n"
            "  -w      Wait forever, continue execution on SIGINT\n"
            "  -h      This help\n"
            "All commands are executed in the given order\n");
}

static ComponentPtr component;

void cmxadm_list_processes()
{
    for (Registry::iterator itReg = Registry::begin(); itReg != Registry::end(); ++itReg)
    {
        ImmutableComponentPtr component = *itReg;
        std::cout << "Component: name=" << component->name() << " pid=" << component->processId() << std::endl;
    }
}

static struct sigaction previous_action;
static int do_wait = 0;

static void cmxadm_wait_sighandler(int _, siginfo_t * __, void * ___)
{
    sigaction(SIGINT, &previous_action, NULL);
    Log::audit(LOG_POSITION(),"Continue");
    do_wait = 0;
}

static void cmxadm_wait()
{
    struct sigaction action;
    action.sa_sigaction = &cmxadm_wait_sighandler;
    sigaction(SIGINT, &action, &previous_action);
    Log::audit(LOG_POSITION(),"Sleep ...........press C-c to continue");
    do_wait = 1;
    while (do_wait)
        sleep(1);
}

int main(int argc, char * argv[])
{
    int opt;

    if (argc == 1)
    {
        cmxadm_help();
        return 0;
    }

    while ((opt = getopt(argc, argv, "rupclwh")) != -1)
    {
        switch (opt)
        {
            case 'r':
                component = Component::create("TestComponent");
                component->newInt64("testvalue") = 1234;
                break;
            case 'u':
                component.reset();
                break;
            case 'p':
                ProcessComponent::update();
                break;
            case 'c':
                Registry::cleanup();
                break;
            case 'l':
                cmxadm_list_processes();
                break;
            case 'w':
                cmxadm_wait();
                break;
            case 'h':
            default:
                cmxadm_help();
                break;
        }
    }
}
