/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <iostream>
#include <sstream>
#include <ctime>
#include <getopt.h>
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/ImmutableComponent.h>
extern "C"
{
#include <fnmatch.h>
}

using namespace cmw::cmx;

const char * timestamp_tostring(uint64_t ts, bool rfc3339)
{
    static char strftime_buf[128];
    ts /= (1000ULL * 1000ULL);
    time_t t = (time_t) ts;
    struct tm tm_time;
    localtime_r(&t, &tm_time);
    if (rfc3339)
    {
        // use rc3339 format
        strftime(strftime_buf, 128, "%FT%TZ%z", &tm_time);
    }
    else
    {
        // use local format (LC_TIME)
        strftime(strftime_buf, 128, "%c", &tm_time);
    }
    return strftime_buf;
}

void output_csv_escape_string(std::ostream & os, const std::string & s)
{
    os << '"';
    for (std::string::const_iterator c = s.begin(); c != s.end(); ++c)
    {
        if (*c == '"')
        {
            os << '"' << '"';
        }
        else
        {
            os << *c;
        }
    }
    os << '"';
}

void output_value_line(std::ostream & os, const std::string::size_type indent, const std::string & s)
{
    os << "value=\"";
    std::string::size_type fragmentLen = /*linewidth*/120 - indent;
    for (std::string::size_type pos = 0; pos < s.size();)
    {
        if (pos != 0) os << std::string(indent, ' '); // indention

        std::string::size_type posFragment;
        for (posFragment = 0; posFragment < fragmentLen && (posFragment + pos) < s.size(); posFragment++)
        {
            const char c = s[pos + posFragment];
            if (c == '\n')
            {
                posFragment++;
                break;
            }
            else if (c >= ' ' && c <= '~')
            { //ascii
                os << c;
            }
            else
            {
                os << '?';
            }
        }
        pos += posFragment;
        if (pos < s.size()) os << std::endl;
    }
    os << "\"" << std::endl;
}

void help()
{
    std::cout << "-h             help" << std::endl;
    std::cout << "-n <name>      filter by component name" << std::endl;
    std::cout << "-p <proces_id> filter by proces id" << std::endl;
    std::cout << "-t             use rfc-3389 time formatting instead of LC_TIME" << std::endl;
    std::cout << "-c             CSV/RFC4180 like formatting (implies -t)" << std::endl;
}

int main(int argc, char *argv[])
{
    int opt;
    bool filter_comp_name = false;
    std::string filter_comp_name_value;
    bool filter_process_id = false;
    int filter_process_id_value;
    bool time_rfc3339 = false;
    bool csv_format = false;

    while ((opt = getopt(argc, argv, "n:p:tch")) != -1)
    {
        switch (opt)
        {
            case 'n':

                filter_comp_name = true;
                filter_comp_name_value = optarg;
                break;
            case 'p':
                filter_process_id = true;
                filter_process_id_value = atoi(optarg);
                break;
            case 't':
                time_rfc3339 = true;
                break;
            case 'c':
                csv_format = true;
                break;
            case 'h':
            default:
                help();
                exit(0);
                break;
        }
    }

    if (csv_format)
    {
        std::cout << "\"component_name\",\"value_name\",\"value_type\",\"mtime\",\"value\"" << std::endl;
    }

    Registry::cleanup();
    for (Registry::iterator itReg = Registry::begin(); itReg != Registry::end(); ++itReg)
    {
        try
        {
            ImmutableComponentPtr component = *itReg;
            if (filter_process_id)
            {
                if (component->processId() != filter_process_id_value)
                {
                    continue;
                }
            }
            if (filter_comp_name)
            {
                if (fnmatch(filter_comp_name_value.c_str(), component->name().c_str(), 0) != 0)
                {
                    continue;
                }
            }

            if (not csv_format)
            {
                std::cout << "Component: " << component->name() << " pid=" << component->processId() << std::endl;
            }
            for (ImmutableComponent::iterator itComp = component->begin(); itComp != component->end(); ++itComp)
            {
                try
                {
                    std::ostringstream line;
                    CmxRef m = *itComp;
                    const std::string & s = component->getValueAsString(m);
                    if (csv_format)
                    {
                        output_csv_escape_string(std::cout, component->name());
                        std::cout << ",";
                        output_csv_escape_string(std::cout, m.name());
                        std::cout << ",";
                        output_csv_escape_string(std::cout, m.type_name());
                        std::cout << ",";
                        output_csv_escape_string(std::cout, timestamp_tostring(m.mtime(), true));
                        std::cout << ",";
                        output_csv_escape_string(std::cout, component->getValueAsString(m));
                        std::cout << std::endl;
                    }
                    else
                    {
                        std::cout << "    name=\"" << m.name() << '"' << std::endl;
                        std::cout << "    mtime=\"" << timestamp_tostring(m.mtime(), time_rfc3339) << '"' << std::endl;
                        std::cout << "    type=\"" << m.type_name() << '"' << std::endl;
                        if (CmxTypeTagString::cmx_type == m.type())
                        {
                            std::cout << "    current-size=" << s.size() << std::endl;
                        }
                        std::cout << "    ";
                        output_value_line(std::cout, 11, s);
                        std::cout << std::endl;
                    }
                }
                catch (CmxException & e)
                {
                    std::cerr << "Exception: " << e.what() << std::endl;
                }
            }
        }
        catch (CmxException & e)
        {
            std::cerr << "Exception in access to component" //
                    << " name=" << itReg.name() //
                    << " process_id=" << itReg.processId() //
                    << ": " << e.what() << std::endl;
        }
    }

    return EXIT_SUCCESS;
}
