/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <sstream>

extern "C"
{
#include <cmw-cmx/shm.h>
#include <cmw-cmx/log.h>
}

cmx_shm * cmx_shm_ptr;

TEST(TestShmSingle, test_single_types)
{
    cmx_shm_value shm_value;
    int value_handle;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));

    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "CMX_TYPE_INT64");
    ASSERT_TRUE(value_handle > 0);
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_FLOAT64, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_BOOL, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_STRING, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, -1, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, __INT_MAX__, &shm_value));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, value_handle));

    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_FLOAT64, "CMX_TYPE_FLOAT64");
    ASSERT_TRUE(value_handle > 0);
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &shm_value));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_FLOAT64, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_BOOL, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_STRING, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, -1, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, __INT_MAX__, &shm_value));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, value_handle));

    value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_BOOL, "CMX_TYPE_BOOL");
    ASSERT_TRUE(value_handle > 0);
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_FLOAT64, &shm_value));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_BOOL, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_STRING, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, -1, &shm_value));
    ASSERT_EQ(E_CMX_TYPE_MISMATCH, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, __INT_MAX__, &shm_value));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, value_handle));

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_STRING, "CMX_TYPE_STRING");
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, value_handle);

        value_handle = cmx_shm_add_value_single(cmx_shm_ptr, -1, "-1");
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, value_handle);

        value_handle = cmx_shm_add_value_single(cmx_shm_ptr, __INT_MAX__, "__INT_MAX__");
        ASSERT_EQ(E_CMX_INVALID_ARGUMENT, value_handle);
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmSingle, add_invalid_arg)
{
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, ""));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT,
                cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "01234567890123456789012345678901234567890"
                        "123456789012345678901234567890123456789"));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_STRING, ""));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_single(NULL, CMX_TYPE_INT64, ""));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_single(NULL, CMX_TYPE_INT64, "test"));

    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}
