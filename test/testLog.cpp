/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*
 * \internal
 * \file
 */
#include <iostream>
#include <gtest/gtest.h>

extern "C"
{
#include <cmw-cmx/log.h>
}

int log_test_level;
char log_test_source_file[1024];
int log_test_line_no;
char log_test_function_name[1024];
char log_test_message[1024];

static void test_log_adapter(enum cmx_log_levels level, const char * source_file, int line_no, const char * function_name,
        const char * fmt, va_list ap)
{
    memset(log_test_source_file, '\0', 1024);
    memset(log_test_function_name, '\0', 1024);
    memset(log_test_message, '\0', 1024);
    log_test_level = level;
    strncpy(log_test_source_file, source_file, 1023);
    log_test_line_no = line_no;
    strncpy(log_test_function_name, function_name, 1023);
    vsnprintf(log_test_message, 1024, fmt, ap);
}

TEST(TestLog, set_log_adapter)
{
    cmx_log_set_adapter(test_log_adapter);
    cmx_log_current_level = LOG_LEVEL_TRACE;

    { // test TRACE
        cmx_log_printf(LOG_LEVEL_TRACE, __FILE__, __LINE__, __FUNCTION__, "TRACE %d %s", 123, "eins zwei drei");
        EXPECT_EQ(LOG_LEVEL_TRACE, log_test_level);
        EXPECT_STREQ(__FILE__, log_test_source_file);
        EXPECT_EQ(__LINE__ - 3, log_test_line_no);
        EXPECT_STREQ(__FUNCTION__, log_test_function_name);
        EXPECT_STREQ("TRACE 123 eins zwei drei", log_test_message);
    }
    { // test DEBUG
        cmx_log_printf(LOG_LEVEL_DEBUG, __FILE__, __LINE__, __FUNCTION__, "DEBUG %d %s", 123, "abc");
        EXPECT_EQ(LOG_LEVEL_DEBUG, log_test_level);
        EXPECT_STREQ("DEBUG 123 abc", log_test_message);
    }
    { // test INFO
        cmx_log_printf(LOG_LEVEL_INFO, __FILE__, __LINE__, __FUNCTION__, "INFO %d %s", 123, "abc");
        EXPECT_EQ(LOG_LEVEL_INFO, log_test_level);
        EXPECT_STREQ("INFO 123 abc", log_test_message);
    }
    { // test WARN
        cmx_log_printf(LOG_LEVEL_WARN, __FILE__, __LINE__, __FUNCTION__, "WARN %d %s", 123, "abc");
        EXPECT_EQ(LOG_LEVEL_WARN, log_test_level);
        EXPECT_STREQ("WARN 123 abc", log_test_message);
    }
    { // test ERROR
        cmx_log_printf(LOG_LEVEL_ERROR, __FILE__, __LINE__, __FUNCTION__, "ERROR %d %s", 123, "abc");
        EXPECT_EQ(LOG_LEVEL_ERROR, log_test_level);
        EXPECT_STREQ("ERROR 123 abc", log_test_message);
    }
    { // test AUDIT
        cmx_log_printf(LOG_LEVEL_AUDIT, __FILE__, __LINE__, __FUNCTION__, "AUDIT %d %s", 123, "abc");
        EXPECT_EQ(LOG_LEVEL_AUDIT, log_test_level);
        EXPECT_STREQ("AUDIT 123 abc", log_test_message);
    }

    // reset
    cmx_log_set_adapter(NULL);

    // test there is no logging to the logadapter anymore
    cmx_log_printf(LOG_LEVEL_TRACE, __FILE__, __LINE__, __FUNCTION__, ".....");
    EXPECT_NE(LOG_LEVEL_TRACE, log_test_level);

    // reset
    cmx_log_current_level = -1;
}

TEST(TestLog, level_name)
{
    EXPECT_STREQ("AUDIT", cmx_log_level_name(LOG_LEVEL_AUDIT));
    EXPECT_STREQ("ERROR", cmx_log_level_name(LOG_LEVEL_ERROR));
    EXPECT_STREQ("WARN", cmx_log_level_name(LOG_LEVEL_WARN));
    EXPECT_STREQ("INFO", cmx_log_level_name(LOG_LEVEL_INFO));
    EXPECT_STREQ("DEBUG", cmx_log_level_name(LOG_LEVEL_DEBUG));
    EXPECT_STREQ("TRACE", cmx_log_level_name(LOG_LEVEL_TRACE));
    EXPECT_STREQ("", cmx_log_level_name((enum cmx_log_levels )9999));
}

TEST(TestLog, from_string)
{
    // default
    EXPECT_EQ(LOG_LEVEL_INFO, cmx_log_from_string(NULL));
    // invalid name
    EXPECT_EQ(LOG_LEVEL_INFO, cmx_log_from_string("asd"));
    EXPECT_EQ(LOG_LEVEL_INFO, cmx_log_from_string("trace"));
    // valid names
    EXPECT_EQ(LOG_LEVEL_AUDIT, cmx_log_from_string("AUDIT"));
    EXPECT_EQ(LOG_LEVEL_ERROR, cmx_log_from_string("ERROR"));
    EXPECT_EQ(LOG_LEVEL_WARN, cmx_log_from_string("WARN"));
    EXPECT_EQ(LOG_LEVEL_INFO, cmx_log_from_string("INFO"));
    EXPECT_EQ(LOG_LEVEL_DEBUG, cmx_log_from_string("DEBUG"));
    EXPECT_EQ(LOG_LEVEL_TRACE, cmx_log_from_string("TRACE"));
}
