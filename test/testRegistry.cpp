/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*
 * \internal
 * \file
 */
#include <iostream>
#include <gtest/gtest.h>
#include <cstring>

extern "C"
{
#include <cmw-cmx/registry.h>
#include <cmw-cmx/shm.h>
#include <stdlib.h>
#include <unistd.h>
}

bool component_info_contains_name(const char * name, cmx_component_info * cci, int len)
{
    for (int i = 0; i < len; i++)
    {
        if (strncmp(name, cci[i].name, CMX_NAME_SZ) == 0)
        {
            return true;
        }
    }
    return false;
}

TEST(TestRegistry, discover)
{
    cmx_component_info * component_info;
    int len;
    cmx_shm * cmx_shm_ptr;
    cmx_registry_cleanup();
    {
        ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
        ASSERT_FALSE(component_info_contains_name("TestComponent_discover1", component_info, len));
        free(component_info);

        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("TestComponent_discover1", &cmx_shm_ptr));
        ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
        ASSERT_TRUE(component_info_contains_name("TestComponent_discover1", component_info, len));
        free(component_info);

        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
        ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
        ASSERT_FALSE(component_info_contains_name("TestComponent_discover1", component_info, len));
        free(component_info);
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
    ASSERT_FALSE(component_info_contains_name("TestComponent_discover2", component_info, len));
    free(component_info);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("TestComponent_discover2", &cmx_shm_ptr));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
    ASSERT_TRUE(component_info_contains_name("TestComponent_discover2", component_info, len));
    free(component_info);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));

    ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
    ASSERT_FALSE(component_info_contains_name("TestComponent_discover2", component_info, len));
    free(component_info);
}

TEST(TestRegistry, cleanup)
{
    cmx_registry_cleanup();

    // create component with probably nonexisting pid
    int registry_length;
    cmx_registry_discover(NULL, &registry_length);

    cmx_shm * cmx_shm_ptr;
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create("TestComponent_cleanup", &cmx_shm_ptr));
    {
        cmx_component_info * component_info;
        int len;
        ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
        ASSERT_EQ(registry_length + 1, len);
        int matches = 0;
        for (int i = 0; i < len; i++)
        {
            if (std::string("TestComponent_cleanup") == std::string(component_info[i].name)
                    and getpid() == component_info[i].process_id) matches++;

        }
        EXPECT_EQ(1, matches);
        free(component_info);
    }
    cmx_shm_destroy(cmx_shm_ptr);

    {
        cmx_component_info * component_info;
        int len = -2;
        ASSERT_EQ(E_CMX_SUCCESS, cmx_registry_discover(&component_info, &len));
        ASSERT_EQ(registry_length, len);
        free(component_info);
    }
}

TEST(TestRegistry, format_file__parse_filename)
{
    char filename_buf[1024];
    int pid = 1234, parsed_pid;
    const char * name = "product-id.HelloWorldComponent";
    char parsed_name[CMX_NAME_SZ];
    cmx_registry_format_filename(filename_buf, pid, name);

    ASSERT_EQ(CMX_TRUE, cmx_registry_parse_filename(filename_buf, &parsed_pid, parsed_name));
    EXPECT_EQ(pid, parsed_pid);
    EXPECT_STREQ(name, parsed_name);
}
