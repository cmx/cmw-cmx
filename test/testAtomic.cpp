/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/*
 * \internal
 * \file
 */
#include <gtest/gtest.h>
#include <cstdio>
#include <cstdlib>
#include <ostream>

extern "C"
{
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>

#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/atomic.h>
}

TEST(TestAtomic, implementation_name)
{
    ASSERT_TRUE(strlen(cmx_atomic_implementation_name()) != 0);
    ASSERT_TRUE(strlen(cmx_atomic_implementation_name()) > 0);
    std::cerr << "cmx/atomic.c implementation_name() = \"" << cmx_atomic_implementation_name() << "\"" << std::endl;
}

// test cas concurrent single threaded

TEST(TestAtomic, cas)
{
    int32_t val = 1010, tmp, *mem = &val;
    ASSERT_EQ(1010, cmx_atomic_read_int32(&val));
    ASSERT_EQ(1010, cmx_atomic_read_int32(mem));

    cmx_atomic_set_int32(&val, 0);
    ASSERT_EQ(0, cmx_atomic_read_int32(&val));
    ASSERT_EQ(0, cmx_atomic_read_int32(mem));

    tmp = 3030;
    ASSERT_TRUE(cmx_atomic_val_compare_and_swap_int32(&val, 0, tmp) == 0);
    ASSERT_TRUE(val == tmp);

    mem = &tmp;
    ASSERT_TRUE(cmx_atomic_val_compare_and_swap_int32(mem, tmp, 3030) == tmp);

    cmx_atomic_smp_mb();
    ASSERT_TRUE(*mem == 3030);
}

// test cas concurrent threads
const int NO_THREADS = 4;
static const int SS_NUM_ENTRIES = 2000;
static const int32_t LOCK_LOCKED = 1;
static const int32_t LOCK_UNLOCKED = 0;
static const int32_t OWNER_ORPHAN = -1;
struct shared_struct_t
{
    int32_t locks[SS_NUM_ENTRIES];
    int32_t owner[SS_NUM_ENTRIES];
};

struct thread_info_t
{
    shared_struct_t * shared_struct;
    pthread_t thread_id;
    int no_lock;
    int no_unlock;
    int no_lock_error;
    int no_unlock_error;
};

void * thread__cas__concurrent(void * arg)
{
    struct thread_info_t * thread_info = (struct thread_info_t *) arg;
    struct shared_struct_t * shared_struct = thread_info->shared_struct;
    int start = time(NULL);
    int no_entries = 0;

    while (time(NULL) - start < 10) // run for 10 secs.
    {
        if (no_entries < (SS_NUM_ENTRIES / NO_THREADS) && (std::rand() % 2 == 0 || no_entries == 0))
        {
            int success = 0;
            // make a entry
            for (int i = 0; i < SS_NUM_ENTRIES; i++)
            {
                if (cmx_atomic_val_compare_and_swap_int32(&shared_struct->locks[i], LOCK_UNLOCKED, LOCK_LOCKED)
                        == LOCK_UNLOCKED)
                {
                    thread_info->no_lock++;

                    // check that data is empty
                    if (cmx_atomic_read_int32(&shared_struct->owner[i]) != OWNER_ORPHAN)
                    {
                        thread_info->no_lock_error++;
                    }

                    cmx_atomic_set_int32(&shared_struct->owner[i], (int32_t) thread_info->thread_id);
//                    std::cout << "added " << cmx_atomic_read_int32(&shared_struct->owner[i]) << " lock "
//                            << cmx_atomic_read_int32(&shared_struct->locks[i]) << std::endl;
                    no_entries++;
                    success = 1;
                    break;
                }
            }
            if (!success)
            {
                thread_info->no_lock_error++;
                std::cerr << (int32_t) thread_info->thread_id << " failed to make a new entry. give up. no_entries="
                        << no_entries << std::endl;
                return NULL;
            }
        }
        else
        {
            int success = 0;
            // remove a entry of this thread
            for (int i = 0; i < SS_NUM_ENTRIES; i++)
            {
                // reset data
                // check if this slot [i] belongs to current thread
                if (cmx_atomic_val_compare_and_swap_int32(&shared_struct->owner[i], (int32_t) thread_info->thread_id,
                        OWNER_ORPHAN) == (int32_t) thread_info->thread_id)
                {
//                    std::cerr << "Unlock " << i << " and i am " << (int32_t) thread_info->thread_id << std::endl;
                    // unlock
                    if (cmx_atomic_val_compare_and_swap_int32(&shared_struct->locks[i], LOCK_LOCKED, LOCK_UNLOCKED)
                            != LOCK_LOCKED)
                    {
                        thread_info->no_unlock_error++;
                        std::cerr << "Invalid read at " << i << " in thread " << (int32_t) thread_info->thread_id
                                << std::endl;
                        return NULL;
                    }
                    success = 1;
                    no_entries--;
                    thread_info->no_unlock++;
                    break;
                }
            }
            if (!success)
            {
                thread_info->no_lock_error++;
                std::cerr << (int32_t) thread_info->thread_id
                        << " failed to remove at least 1 entry. give up. no_entries=" << no_entries << std::endl;
                return NULL;
            }
        }
    }

    return NULL;
}

TEST(TestAtomic, cas__concurrent_threaded)
{
    struct thread_info_t thread_infos[NO_THREADS];
    struct shared_struct_t shared_struct;

    for (int i = 0; i < SS_NUM_ENTRIES; i++)
    {
        shared_struct.locks[i] = LOCK_UNLOCKED;
        shared_struct.owner[i] = OWNER_ORPHAN;
    }

    std::srand(time(NULL));

    for (int i = 0; i < NO_THREADS; i++)
    {
        thread_infos[i].shared_struct = &shared_struct;
        thread_infos[i].no_lock = thread_infos[i].no_lock_error = 0;
        thread_infos[i].no_unlock = thread_infos[i].no_unlock_error = 0;
        pthread_create(&thread_infos[i].thread_id, NULL, &thread__cas__concurrent, &thread_infos[i]);
    }
    for (int i = 0; i < NO_THREADS; i++)
    {
        pthread_join(thread_infos[i].thread_id, NULL);
        std::ostringstream threadname;
        threadname << "Thread [" << i << "]";
        EXPECT_GT(thread_infos[i].no_lock,0) << threadname.str();
        EXPECT_GT(thread_infos[i].no_unlock,0) << threadname.str();
        EXPECT_EQ(0,thread_infos[i].no_lock_error) << threadname.str();
        EXPECT_EQ(0,thread_infos[i].no_unlock_error) << threadname.str();
    }
}

TEST(TestAtomic, read)
{
    int32_t val32s = -10;
    uint16_t val16u = 10U;

    ASSERT_EQ(-10, cmx_atomic_read_int32(&val32s));
    ASSERT_EQ(10U, cmx_atomic_read_uint16(&val16u));
}

TEST(TestAtomic, set)
{
    int32_t val32s = -10;
    uint16_t val16u = 10U;

    cmx_atomic_set_int32(&val32s, (int) 0xfefefefe);
    ASSERT_EQ((int )0xfefefefe, val32s);

    cmx_atomic_set_uint16(&val16u, 0xfefe);
    ASSERT_EQ(0xfefe, val16u);
}

TEST(TestAtomic, mb)
{
    // since memory barriers have no easily measurable effect
    // this test only checks if the function is available.
    cmx_atomic_smp_mb();
    cmx_atomic_smp_rmb();
    cmx_atomic_smp_wmb();
}

#ifdef __arm__
// Concurrent threads with memory barrires
// are expected to succeed
unsigned int arm_with_mb__a;
unsigned int arm_with_mb__b;

void * arm_with_mb__writer(void * threadarg)
{
    while (*(int*)(threadarg))
    {
        cmx_atomic_set_int32(&arm_with_mb__a, arm_with_mb__a+1); // a++
        cmx_atomic_smp_mb();// Force writing of b after writing a
        cmx_atomic_set_int32(&arm_with_mb__b, arm_with_mb__b+1);// b++
    }
    printf("done %s\n", __FUNCTION__);
    return NULL;
}

void * arm_with_mb__reader(void * threadarg)
{
    for (unsigned int i = 0; i < 100000000U; i++)
    {
        unsigned int b = cmx_atomic_read_int32(&arm_with_mb__b);
        cmx_atomic_smp_mb(); // Force read from a after read b
        unsigned int a = cmx_atomic_read_int32(&arm_with_mb__a);
        if (a < b)
        { // b got reordered before a
            *(int*)threadarg = 0;
            printf("failed %s with i = %u a = %d b = %d\n", __FUNCTION__, i, a, b);
            return NULL;
        }
    }
    printf("done %s\n", __FUNCTION__);
    return NULL;
}

TEST(TestAtomic, mb_arm_with)
{
    pthread_t thread1, thread2, thread3, thread4;
    int run_writer = 1;
    int read_success = 1;
    arm_with_mb__a = arm_with_mb__b = 0;

    pthread_create(&thread1, NULL, arm_with_mb__writer, (void*)&run_writer);
    pthread_create(&thread2, NULL, arm_with_mb__reader, (void*)&read_success);
    pthread_create(&thread3, NULL, arm_with_mb__reader, (void*)&read_success);
    pthread_create(&thread4, NULL, arm_with_mb__reader, (void*)&read_success);

    pthread_join(thread4, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread2, NULL);
    run_writer = 0;
    pthread_join(thread1, NULL);

    ASSERT_TRUE(read_success);
}

// Concurrent threads without memory barriers
// are expected to fail on arm
unsigned int arm_without_mb__a;
unsigned int arm_without_mb__b;

void * arm_without_mb__writer(void * threadarg)
{
    while (*(int*)(threadarg))
    {
        // inline assember ensures that the stores
        // happen in instructions after each other
        __asm__ volatile(
                "ldr r1, [%[a_ptr],#0]\n\t"
                "ldr r2, [%[b_ptr],#0]\n\t"
                "add r1, #1\n\t"
                "add r2, #1\n\t"
                "str r1, [%[a_ptr], #0]\n\t"
                "str r2, [%[b_ptr], #0]\n\t"
                :
                : [a_ptr] "r" (&arm_without_mb__a), [b_ptr] "r" (&arm_without_mb__b)
                : "r1","r2"
        );
    }
    return NULL;
}

void * arm_without_mb__reader(void * threadarg)
{
    int * const a_ptr = &arm_without_mb__a;
    int * const b_ptr = &arm_without_mb__b;
    for (unsigned int i = 0; i < 100000000U; i++)
    {
        if (*a_ptr < *b_ptr)
        { // b got reordered before a
            *(int*)threadarg = 0;
            //printf("failed %s with i = %u a = %d b = %d (values may be different)\n", __FUNCTION__, i, *a_ptr, *b_ptr);
            return NULL;
        }
    }
    printf("not failed! %s\n", __FUNCTION__);
    return NULL;
}

TEST(TestAtomic, mb_arm_without)
{
    pthread_t thread1, thread2, thread3, thread4;
    int run_writer = 1;
    volatile int read_success = 1;
    arm_without_mb__a = arm_without_mb__b = 0;

    pthread_create(&thread1, NULL, arm_without_mb__writer, (void*)&run_writer);
    pthread_create(&thread2, NULL, arm_without_mb__reader, (void*)&read_success);
    pthread_create(&thread3, NULL, arm_without_mb__reader, (void*)&read_success);
    pthread_create(&thread4, NULL, arm_without_mb__reader, (void*)&read_success);

    pthread_join(thread4, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread2, NULL);
    run_writer = 0;
    pthread_join(thread1, NULL);

    ASSERT_FALSE(read_success);
}
#endif // __arm__

#if defined(__x86_64__)
// Concurrent threads without memory barriers
// are expected to succeed on x86
unsigned int x86_without_mb__a;
unsigned int x86_without_mb__b;

void * x86_without_mb__writer(void * threadarg)
{
    while (*(int*) (threadarg))
    {
        __asm__ volatile(
                "addl $1, (%[ptr_a])\n\t" // a++
                "addl $1, (%[ptr_b])\n\t"// b++
                :
                : [ptr_a] "r" (&x86_without_mb__a), [ptr_b] "r" (&x86_without_mb__b)
                : "memory");
    }
    return NULL;
}

void * x86_without_mb__reader(void * threadarg)
{
    for (unsigned int i = 0; i < 200000000U; i++)
    {
        asm goto(
                "movl (%[ptr_b]), %%eax\n\t" //read b
                "cmp %%eax, (%[ptr_a])\n\t"// compare  b,a -> intel: a,b
                "jl %l[fail]\n\t"// jump if a < b
                "jmp %l[success]\n\t"// else
                : /**/
                : [ptr_a] "r" (&x86_without_mb__a), [ptr_b] "r" (&x86_without_mb__b)
                : "%eax" : fail, success);
        __builtin_unreachable();

        success: //
        continue;
        fail: //fail
        *(int*) threadarg = 0;
        printf("failed %s with i = %u a = %u b = %u\n", __FUNCTION__, i, x86_without_mb__a, x86_without_mb__b);
        return NULL;
    }
    return NULL;
}

TEST(TestAtomic, mb_x86_without)
{
    pthread_t thread1, thread2, thread3, thread4;
    int run_writer = 1;
    int read_success = 1;
    x86_without_mb__a = x86_without_mb__b = 0;

    pthread_create(&thread1, NULL, x86_without_mb__writer, (void*) &run_writer);
    pthread_create(&thread2, NULL, x86_without_mb__reader, (void*) &read_success);
    pthread_create(&thread3, NULL, x86_without_mb__reader, (void*) &read_success);
    pthread_create(&thread4, NULL, x86_without_mb__reader, (void*) &read_success);

    pthread_join(thread4, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread2, NULL);
    run_writer = 0;
    pthread_join(thread1, NULL);

    ASSERT_TRUE(read_success);
}
#endif


