/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <sstream>

extern "C"
{
#include <cmw-cmx/shm.h>
#include <cmw-cmx/log.h>
}

///////////////////////////////

// the 'random' here is very naive
static std::string generate_random_string(int length, int seed)
{
    std::ostringstream os;
    for (int i = 0; i < length; i++)
    {
        os << (char) ('0' + (seed + i) % 10);
        seed = (seed * i) % 20000;
    }
    return os.str();
}

TEST(TestShmString, test1)
{
    std::ostringstream os;
    cmx_shm * cmx_shm_ptr;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));

    int string_metric = cmx_shm_add_value_string(cmx_shm_ptr, "TestString1", CMX_SHM_MAX_STRING_SZ);
    ASSERT_TRUE(string_metric >= 0);

    for (size_t i = 0; i < CMX_SHM_MAX_STRING_SZ + 1000; i += 1 + (i % 11))
    {
        std::string s(os.str());
        if (s.size() <= CMX_SHM_MAX_STRING_SZ)
        {
            ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_string(cmx_shm_ptr, string_metric, s.c_str(), s.size()))<< "Pos: " << s.size();
        }
        else
        {
            LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
            {
                ASSERT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_set_value_string(cmx_shm_ptr, string_metric, s.c_str(), s.size())) << "Pos: " << s.size();
            }
        }
        while (os.str().size() < i)
            os << 'A';
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

typedef void (*cmx_string_callback)(const char * buf, size_t len, void * userarg);
static void string_cb(const char * buf, size_t len, void * userarg)
{
    std::string & tempString = *(std::string*) userarg;
    tempString.append(buf, len);
}

TEST(TestShmString, test2)
{
    cmx_shm * cmx_shm_ptr = NULL;
    const std::string randomString1(generate_random_string(CMX_SHM_MAX_STRING_SZ, 12345));
    const std::string randomString2(generate_random_string(CMX_SHM_MAX_STRING_SZ, 52431));
    std::string tempString;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    int string1_metric = cmx_shm_add_value_string(cmx_shm_ptr, "randomString2", CMX_SHM_MAX_STRING_SZ);
    ASSERT_TRUE(string1_metric >= 0);
    int string2_metric = cmx_shm_add_value_string(cmx_shm_ptr, "randomString2", CMX_SHM_MAX_STRING_SZ);
    ASSERT_TRUE(string2_metric >= 0);

    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_set_value_string(cmx_shm_ptr, string1_metric, randomString1.c_str(), randomString1.size()));
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_set_value_string(cmx_shm_ptr, string2_metric, randomString2.c_str(), randomString2.size()));

    tempString.clear();
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_get_value_string(cmx_shm_ptr, string1_metric, string_cb, (void* )&tempString,NULL));
    ASSERT_EQ(randomString1, tempString);
    ASSERT_NE(randomString2, tempString);

    tempString.clear();
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_get_value_string(cmx_shm_ptr, string2_metric, string_cb, (void* )&tempString,NULL));
    ASSERT_NE(randomString1, tempString);
    ASSERT_EQ(randomString2, tempString);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, string2_metric));
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, string1_metric));

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmString, test3)
{
    cmx_shm * cmx_shm_ptr = NULL;
    const std::string stringValue1(generate_random_string(56, 56674));
    const std::string stringValue2(generate_random_string(56, 12345));
    std::string tempString;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    // add
    int string_metric1 = cmx_shm_add_value_string(cmx_shm_ptr, "randomString", 56);
    ASSERT_TRUE(string_metric1 >= 0);
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_set_value_string(cmx_shm_ptr, string_metric1, stringValue1.c_str(), stringValue1.size()));

    //remove string_metric1
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, string_metric1));

    // assert fail
    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
        ASSERT_EQ(E_CMX_INVALID_HANDLE,
                cmx_shm_get_value_string(cmx_shm_ptr, string_metric1, string_cb, (void* )&tempString,NULL));

    // add new one
    int string_metric2 = cmx_shm_add_value_string(cmx_shm_ptr, "randomString", 56);
    ASSERT_TRUE(string_metric2 >= 0);
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_set_value_string(cmx_shm_ptr, string_metric2, stringValue2.c_str(), stringValue2.size()));

    // still fail
    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
        ASSERT_EQ(E_CMX_INVALID_HANDLE,
                cmx_shm_get_value_string(cmx_shm_ptr, string_metric1, string_cb, (void* )&tempString,NULL));

    // but success with string_metric2
    tempString.clear();
    ASSERT_EQ(E_CMX_SUCCESS,
            cmx_shm_get_value_string(cmx_shm_ptr, string_metric2, string_cb, (void* )&tempString,NULL));
    ASSERT_EQ(stringValue2, tempString);
    ASSERT_NE(stringValue1, tempString);

    //remove string_metric1
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, string_metric2));

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmString, add_remove_loop)
{
    const char * test_string = "01234567890123456789012345678901234567890123456789" // 50
                    "01234567890123456789012345678901234567890123456789"// 100
                    "01234567890123456789012345678901234567890123456789"// 150
                    "01234567890123456789012345678901234567890123456789"// 200
                    "01234567890123456789012345678901234567890123456789"// 250
                    "01234567890123456789012345678901234567890123456789"// 300
                    "01234567890123456789012345678901234567890123456789"// 350
                    "01234567890123456789012345678901234567890123456789";// 400
    const int TEST_STRING_LENGTH = 400;

    int string_metric;
    cmx_shm * cmx_shm_ptr = NULL;

    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    for (int i = 0; i < CMX_NUMBER_OF_VALUES; ++i)
    {
        //add
        string_metric = cmx_shm_add_value_string(cmx_shm_ptr, "randomString", TEST_STRING_LENGTH);
        ASSERT_TRUE(string_metric > 0);
        //set
        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_string(cmx_shm_ptr, string_metric, test_string, TEST_STRING_LENGTH));
        //remove
        ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, string_metric));
    }
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmString, add_invalid_arg)
{
    cmx_shm * cmx_shm_ptr = NULL;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_string(NULL, "test", 123));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_string(cmx_shm_ptr, "", 0));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_string(cmx_shm_ptr, "test", -1));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT, cmx_shm_add_value_string(cmx_shm_ptr, "", 123));
        EXPECT_EQ(E_CMX_INVALID_ARGUMENT,
                cmx_shm_add_value_string(cmx_shm_ptr, "01234567890123456789012345678901234567890123456789" // 50
                                "01234567890123456789012345678901234567890123456789"//100
                , 123));
    }

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}
