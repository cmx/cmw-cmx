/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file \u201cCOPYING\u201d.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>

extern "C"
{
#include <cmw-cmx/shm.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>
#include <cmw-cmx/common.h>
#include <cmw-cmx/log.h>
}

static cmx_shm * cmx_shm_ptr;

TEST(TestShmPrivate, testRemoveInvalidPid)
{
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    int pid = cmx_shm_ptr->process_id;
    ASSERT_EQ(pid, getpid());

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        cmx_shm_ptr->process_id++;
        ASSERT_EQ(E_CMX_INVALID_PID, cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TEST"));
    }

    cmx_shm_ptr->process_id--;
    int value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TEST");
    ASSERT_TRUE(value_handle > 0);

    LOG_SET_LEVEL(LOG_LEVEL_AUDIT)
    {
        cmx_shm_ptr->process_id++;
        ASSERT_EQ(E_CMX_INVALID_PID, cmx_shm_remove_value(cmx_shm_ptr, value_handle));
    }

    cmx_shm_ptr->process_id--;
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_remove_value(cmx_shm_ptr, value_handle));

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmPrivate, testSingleTxnCounter)
{
    cmx_shm_value shm_value;
    shm_value._int64 = 123;
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    int value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, "TEST");
    ASSERT_TRUE(value_handle > 0);

    int txn1 = cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle)].value.txn;

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_single(cmx_shm_ptr, value_handle, CMX_TYPE_INT64, &shm_value));

    int txn2 = cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle)].value.txn;

    ASSERT_NE(txn1, txn2);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

TEST(TestShmPrivate, testStringTxnCounter)
{
    std::string name(test_info_->test_case_name() + std::string(".") + test_info_->name());
    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_create(name.c_str(), &cmx_shm_ptr));
    ASSERT_TRUE(cmx_shm_ptr != NULL);

    int value_handle = cmx_shm_add_value_string(cmx_shm_ptr, "TEST", 10);
    ASSERT_TRUE(value_handle > 0);

    int txn1 = cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle)].value.txn;

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_set_value_string(cmx_shm_ptr, value_handle, "1234", 4));

    int txn2 = cmx_shm_ptr->value[cmx_shm_unpack_index(value_handle)].value.txn;

    ASSERT_NE(txn1, txn2);

    ASSERT_EQ(E_CMX_SUCCESS, cmx_shm_destroy(cmx_shm_ptr));
}

