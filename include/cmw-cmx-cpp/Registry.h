/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_REGISTRY
#define HAVE_CMW_CMX_REGISTRY

#include <boost/shared_ptr.hpp>
#include <vector>
extern "C"
{
#include <stdint.h>
}

#include "ImmutableComponent.h"
#include "Component.h"

namespace cmw
{
namespace cmx
{

/*
 * \brief Predicate to filter Components based on name and/or process id
 *
 * for use with algorithms like std::find_if().
 * \code
 * ComponentIterator it = std::find_if(component->begin(), component->end(), ComponentPredicate::name(value_name));
 * \endcode
 */
struct ComponentPredicate : std::unary_function<const ImmutableComponentPtr &, bool>
{
    ComponentPredicate::result_type operator()(ComponentPredicate::argument_type immutableComponent) const;

    static ComponentPredicate name(const std::string & name)
    {
        return ComponentPredicate(name, 0, true, false);
    }
    static ComponentPredicate processId(const int processId)
    {
        return ComponentPredicate("", processId, false, true);
    }
    static ComponentPredicate nameAndProcessId(const std::string & name, const int processId)
    {
        return ComponentPredicate(name, processId, true, true);
    }
private:
    ComponentPredicate(const std::string name, const int processId, bool matchName, bool matchProcessId) :
                    name_(name),
                    processId_(processId),
                    matchName_(matchName),
                    matchProcessId_(matchProcessId)
    {
    }
    const std::string name_;
    const int processId_;
    const bool matchName_;
    const bool matchProcessId_;
};

// this datastructure is used to keep a list of components.
// it is of no use for the library user and will be invisible in future.
struct ComponentInfo;
typedef boost::shared_ptr<ComponentInfo> ComponentInfoPtr;

/**
 * \brief RegistryIterator. Iterates over all components on the machine.
 */
class RegistryIterator : public std::iterator<std::input_iterator_tag, ImmutableComponentPtr>
{
public:
    RegistryIterator();

    /**
     * \brief get Component at current position_
     * \throws \see cmx_shm_open()
     */
    value_type operator*();

    RegistryIterator & operator++(); // ++operator

    RegistryIterator operator++(int); // operator++

    bool operator==(const RegistryIterator& __x) const
    {
        return (position_ == __x.position_);
    }

    bool operator!=(const RegistryIterator& __x) const
    {
        return not (position_ == __x.position_);
    }

    std::string name();

    int processId();

    static RegistryIterator end();
private:
    static ComponentInfoPtr discover();
    RegistryIterator(int position, ComponentInfoPtr componentInfo) :
                    position_(position),
                    componentInfo_(componentInfo)
    {
    }
    size_t position_;
    ComponentInfoPtr componentInfo_;
};

/**
 * \brief Registry class helps to list and cleanup Components.
 */
class Registry
{
public:
    typedef RegistryIterator iterator;

    /**
     * \brief returns a iterator to the first component on the machine.
     *
     * The iterator is in fact coupled with a snapshot of all component at the time this
     * method is called. In case Components are destroyed they will still be in the list
     * but access fails.
     */
    static iterator begin()
    {
        return iterator();
    }

    /**
     * \brief returns a iterator that points after the last component on the machine.
     */
    static iterator end()
    {
        return iterator::end();
    }

    /**
     * \brief Directly open a component.
     * \throws \see cmx_shm_open
     */
    static ImmutableComponentPtr open(const int processId, const std::string & componentName);

    /**
     * \brief Returns the number of currently registered components.
     */
    static int count();

    /**
     * \brief Remove components leftover from dead processes.
     */
    static void cleanup();
};
} // namespace cmx
} // namespace cmw
#endif
