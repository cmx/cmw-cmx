/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_CMXEXCEPTION
#define HAVE_CMW_CMX_CMXEXCEPTION

#include <stdexcept>

namespace cmw
{
namespace cmx
{

/// CMX base exception.
struct CmxException : public std::runtime_error
{
public:
    /// @param msg Message of the exception
    CmxException(const std::string & msg) :
                    std::runtime_error(msg)
    {
    }
};

struct CmxTypeCastException : public CmxException
{
public:
    CmxTypeCastException(const int from_type, const int to_type) :
                    CmxException("Invalid type conversion attempted"),
                    from_type(from_type),
                    to_type(to_type)
    {
    }
    const int from_type;
    const int to_type;
};

/// base class for all more specific exceptions
/// translated from C error codes.
struct CmxErrorCodeException : public CmxException
{
    CmxErrorCodeException(int errorCode);
    const int errorCode_;
};

/// E_CMX_OPERATION_FAILED
struct CmxOperationFailedException : public CmxErrorCodeException
{
    CmxOperationFailedException();
};

/// \brief E_CMX_INVALID_HANDLE
struct CmxInvalidHandleException : public CmxErrorCodeException
{
    CmxInvalidHandleException();
};

/// E_CMX_TYPE_MISMATCH
struct CmxTypeMismatchException : public CmxErrorCodeException
{
    CmxTypeMismatchException();
};

/// E_CMX_CORRUPT_SEGMENT
struct CmxCorruptSegmentException : public CmxErrorCodeException
{
    CmxCorruptSegmentException();
};

/// E_CMX_INVALID_ARGUMENT
struct CmxInvalidArgumentException : public CmxErrorCodeException
{
    CmxInvalidArgumentException();
};

/// E_CMX_OUT_OF_MEMORY
struct CmxOutOfMemoryException : public CmxErrorCodeException
{
    CmxOutOfMemoryException();
};

/// E_CMX_CREATE_FAILED
struct CmxCreateFailedException : public CmxErrorCodeException
{
    CmxCreateFailedException();
};

/// E_CMX_INVALID_PID
struct CmxInvalidPIDException : public CmxErrorCodeException
{
    CmxInvalidPIDException();
};

/// E_CMX_CONCURRENT_MODIFICATION
struct CmxConcurrentModificationException : public CmxErrorCodeException
{
    CmxConcurrentModificationException();
};

/// E_CMX_NAME_EXISTS
struct CmxNameExistsException : public CmxErrorCodeException
{
    CmxNameExistsException();
};

/// E_CMX_NOT_FOUND
struct CmxNotFoundException : public CmxErrorCodeException
{
    CmxNotFoundException();
};

/// E_CMX_COMPONENT_FULL
struct CmxComponentFullException : public CmxErrorCodeException
{
    CmxComponentFullException();
};

/// E_CMX_PROTOCOL_VERSION
struct CmxProtocolVersionException : public CmxErrorCodeException
{
    CmxProtocolVersionException();
};

/// E_CMX_OUT_OF_RANGE
struct CmxOutOfRangeException : public CmxErrorCodeException
{
    CmxOutOfRangeException();
};

} // namespace cmx
} // namespace cmw

#endif // CMW_CMX_EXCEPTION_INCLUDED
