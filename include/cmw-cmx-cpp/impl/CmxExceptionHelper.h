/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#ifndef HAVE_CMW_CMX_CMXEXCEPTIONHELPER
#define HAVE_CMW_CMX_CMXEXCEPTIONHELPER

#include <sstream>
#include <cmw-cmx-cpp/CmxException.h>


#define CMX_THROW_EXCEPTION(returncode) \
    switch (returncode) \
    { \
        case E_CMX_OPERATION_FAILED:\
            throw CmxOperationFailedException();\
        case E_CMX_INVALID_HANDLE:\
            throw CmxInvalidHandleException();\
        case E_CMX_TYPE_MISMATCH:\
            throw CmxTypeMismatchException();\
        case E_CMX_CORRUPT_SEGMENT:\
            throw CmxCorruptSegmentException();\
        case E_CMX_INVALID_ARGUMENT:\
            throw CmxInvalidArgumentException();\
        case E_CMX_OUT_OF_MEMORY:\
            throw CmxOutOfMemoryException();\
        case E_CMX_CREATE_FAILED:\
            throw CmxCreateFailedException();\
        case E_CMX_INVALID_PID:\
            throw CmxInvalidPIDException();\
        case E_CMX_CONCURRENT_MODIFICATION:\
            throw CmxConcurrentModificationException();\
        case E_CMX_NAME_EXISTS:\
            throw CmxNameExistsException();\
        case E_CMX_NOT_FOUND:\
            throw CmxNotFoundException();\
        case E_CMX_COMPONENT_FULL:\
            throw CmxComponentFullException();\
        case E_CMX_PROTOCOL_VERSION:\
            throw CmxProtocolVersionException();\
        case E_CMX_OUT_OF_RANGE:\
            throw CmxOutOfRangeException();\
        default:\
            std::ostringstream os; \
            os << "Unknown error-code: " << returncode; \
            os << " (" << cmx_common_strerror(returncode) << ")"; \
            throw CmxException(os.str());\
    }

#define CMX_THROW_EXCEPTION_IF(returncode) \
    if (returncode != E_CMX_SUCCESS) { CMX_THROW_EXCEPTION(returncode) }


#endif // HAVE_CMW_CMX_CMXEXCEPTIONHELPER
