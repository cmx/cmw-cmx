/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
/** \file
 *  \brief CMX Parent include file
 */
#ifndef HAVE_CMW_CMX_C_CMX_H
#define HAVE_CMW_CMX_C_CMX_H

// CMX shared memory Components and values
#include <cmw-cmx/shm.h>
// CMX Logging and logging redirection functions
#include <cmw-cmx/log.h>
// Predefined metrics in CMX Process Component
#include <cmw-cmx/process.h>
// Lookup of CMX Components
#include <cmw-cmx/registry.h>

#endif // HAVE_CMW_CMX_C_CMX_H
