/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <algorithm>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/Log.h>
#include <cmw-cmx-cpp/CmxTypes.h>
#include <cmw-cmx-cpp/Registry.h>

extern "C"
{
#include <cmw-cmx/common.h>
}

using namespace std;
using namespace cmw::cmx;

TEST(TestComponent, get_value)
{
    Registry::cleanup();
    int count = Registry::count();

    ASSERT_LE(count, Registry::count())<< "Start with empty component registry";
    {
        ComponentPtr c = Component::create("product-id.topic.testcomponent");
        ASSERT_LE(count + 1, Registry::count())<< "Assert that there is now exactly one component";

        CmxInt64 metric1 = c->newInt64("metric1");
        CmxFloat64 metric2 = c->newFloat64("metric2");
        CmxBool metric3 = c->newBool("metric3");
        CmxString metric4 = c->newString("fooo", 123);

        metric1 = 123;
        metric2 = 123.456;
        metric3 = true;
        metric4 = "abc1234!@$#%&^*()";
        EXPECT_EQ(123, (int )metric1);
        EXPECT_EQ(123.456, (double )metric2);
        EXPECT_EQ(true, (bool )metric3);
        EXPECT_EQ("abc1234!@$#%&^*()", (std::string )metric4);

        metric1 = 567;
        metric2 = 567.123;
        metric3 = false;
        metric4 = "213afaDSAD  ad12";
        EXPECT_EQ(567, (int )metric1);
        EXPECT_EQ(567.123, (double )metric2);
        EXPECT_FALSE((bool )metric3);
        EXPECT_EQ("213afaDSAD  ad12", (std::string )metric4);

    }
    ASSERT_LE(count, Registry::count())<< "check that Component 'c' has been automatically destroyed";
}

TEST(TestComponent, set_value)
{
    Registry::cleanup();
    int count = Registry::count();

    ASSERT_LE(count, Registry::count())<< "Start with empty component registry";
    {
        ComponentPtr c = Component::create("product-id.topic.testcomponent");
        ASSERT_LE(count + 1, Registry::count())<< "Assert that there is now exactly one component";

        CmxInt64 metric1 = c->newInt64("metric1");
        CmxFloat64 metric2 = c->newFloat64("metric2");
        CmxBool metric3 = c->newBool("metric3");
        CmxString metric4 = c->newString("fooo", 123);

        EXPECT_EQ(123, metric1 = 123);
        EXPECT_EQ(123.456, metric2 = 123.456);
        EXPECT_EQ(true, metric3 = true);
        EXPECT_EQ("abc1234!@$#%&^*()", metric4 = "abc1234!@$#%&^*()");
    }
    ASSERT_LE(count, Registry::count())<< "check that Component 'c' has been automatically destroyed";
}

TEST(TestComponent, remove)
{
    Registry::cleanup();
    int count = Registry::count();

    Log::setLogLevel(Log::level::AUDIT);
    ASSERT_LE(count, Registry::count())<< "Start with empty component registry";
    {
        ComponentPtr c = Component::create("product-id.topic.testcomponent");
        ASSERT_LE(count + 1, Registry::count())<< "Assert that there is now exactly one component";

        CmxInt64 metric1 = c->newInt64("metric1");
        CmxFloat64 metric2 = c->newFloat64("metric2");
        CmxBool metric3 = c->newBool("metric3");
        CmxString metric4 = c->newString("fooo", 123);

        EXPECT_EQ(123, metric1 = 123);
        EXPECT_EQ(123.456, metric2 = 123.456);
        EXPECT_EQ(true, metric3 = true);
        EXPECT_EQ("abc1234!@$#%&^*()", metric4 = "abc1234!@$#%&^*()");

        c->remove(metric1);
        EXPECT_THROW(metric1 = 123, CmxInvalidHandleException);
        EXPECT_EQ(123.456, metric2 = 123.456);
        EXPECT_EQ(true, metric3 = true);
        EXPECT_EQ("abc1234!@$#%&^*()", metric4 = "abc1234!@$#%&^*()");

        c->remove(metric2);
        EXPECT_THROW(metric2 = 123.456, CmxInvalidHandleException);
        EXPECT_EQ(true, metric3 = true);
        EXPECT_EQ("abc1234!@$#%&^*()", metric4 = "abc1234!@$#%&^*()");

        c->remove(metric3);
        EXPECT_THROW(metric3 = true, CmxInvalidHandleException);
        EXPECT_EQ("abc1234!@$#%&^*()", metric4 = "abc1234!@$#%&^*()");

        c->remove(metric4);
        EXPECT_THROW(metric4 = "abc1234!@$#%&^*()", CmxInvalidHandleException);

        EXPECT_THROW(metric1 = 123, CmxInvalidHandleException);
        EXPECT_THROW(metric2 = 123.456, CmxInvalidHandleException);
        EXPECT_THROW(metric3 = true, CmxInvalidHandleException);
        EXPECT_THROW(metric4 = "abc1234!@$#%&^*()", CmxInvalidHandleException);
    }
    Log::setLogLevel(-1);
    ASSERT_LE(count, Registry::count())<< "check that Component 'c' has been automatically destroyed";
}

TEST(TestComponent, ignoreError_create_set)
{
    Log::setLogLevel(Log::level::AUDIT);
    ComponentPtr dummyComponent = Component::create("", true);
    Log::setLogLevel(-1);
    ASSERT_TRUE(dummyComponent->isDummyMode());

    ASSERT_EQ(0, dummyComponent->newInt64("asdf").value_handle_);
    ASSERT_EQ(0, dummyComponent->newFloat64("asdf").value_handle_);
    ASSERT_EQ(0, dummyComponent->newBool("asdf").value_handle_);
    ASSERT_EQ(0, dummyComponent->newString("asdf", 123).value_handle_);

    ASSERT_EQ(123, dummyComponent->newInt64("abc") = 123);
    ASSERT_EQ(123.123, dummyComponent->newFloat64("abc") = 123.123);
    ASSERT_EQ(true, dummyComponent->newBool("abc") = true);
    ASSERT_EQ("defg", dummyComponent->newString("abc", 12) = "defg");

    ASSERT_EQ(dummyComponent->end(), dummyComponent->begin());

    ASSERT_EQ(0ULL, dummyComponent->getMTime(CmxString(dummyComponent)));
    ASSERT_EQ("", dummyComponent->getName(CmxString(dummyComponent)));

    CmxInt64 m1 = dummyComponent->newInt64("a");
    ASSERT_EQ("", m1.name());
    ASSERT_EQ(CmxTypeTagInt64::cmx_type, m1.type());
    ASSERT_EQ(0ULL, m1.mtime());
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagInt64>::neutral, (CmxTypeInfo<CmxTypeTagInt64>::c_type )m1);

    CmxFloat64 m2 = dummyComponent->newFloat64("a");
    ASSERT_EQ("", m2.name());
    ASSERT_EQ(CmxTypeTagFloat64::cmx_type, m2.type());
    ASSERT_EQ(0ULL, m2.mtime());
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagFloat64>::neutral, (CmxTypeInfo<CmxTypeTagFloat64>::c_type )m2);

    CmxBool m3 = dummyComponent->newBool("a");
    ASSERT_EQ("", m3.name());
    ASSERT_EQ(CmxTypeTagBool::cmx_type, m3.type());
    ASSERT_EQ(0ULL, m3.mtime());
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagBool>::neutral, (CmxTypeInfo<CmxTypeTagBool>::c_type )m3);

    CmxString m4 = dummyComponent->newString("a", 123);
    ASSERT_EQ("", m4.name());
    ASSERT_EQ(CmxTypeTagString::cmx_type, m4.type());
    ASSERT_EQ(0ULL, m4.mtime());
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagString>::neutral, (CmxTypeInfo<CmxTypeTagString>::c_type )m4);
}

/// add four metric, check name, last update and value
TEST(TestComponent, mtime)
{
    ComponentPtr comp = Component::create("test");

    CmxInt64 m1 = comp->newInt64("m1");
    CmxInt64 m2 = comp->newInt64("m2");
    CmxInt64 m3 = comp->newInt64("m3");
    CmxInt64 m4 = comp->newInt64("m4");

    unsigned long long ts_start = cmx_common_current_time_usec();
    m1 = 1;
    m2 = 2;
    m3 = 3;
    m4 = 4;
    unsigned long long ts_end = cmx_common_current_time_usec();

    EXPECT_EQ("m1", m1.name());
    EXPECT_EQ("m2", m2.name());
    EXPECT_EQ("m3", m3.name());
    EXPECT_EQ("m4", m4.name());

    EXPECT_EQ(1, (int )m1);
    EXPECT_EQ(2, (int )m2);
    EXPECT_EQ(3, (int )m3);
    EXPECT_EQ(4, (int )m4);

    EXPECT_LE(ts_start, m1.mtime());
    EXPECT_GE(ts_end, m1.mtime());

    EXPECT_LE(ts_start, m2.mtime());
    EXPECT_GE(ts_end, m2.mtime());

    EXPECT_LE(ts_start, m3.mtime());
    EXPECT_GE(ts_end, m3.mtime());

    EXPECT_LE(ts_start, m4.mtime());
    EXPECT_GE(ts_end, m4.mtime());
}

TEST(TestComponent,dummy)
{
    ComponentPtr dummy = Component::dummy();
    CmxInt64 mInt64 = dummy->newInt64("TestInt64");
    CmxFloat64 mFloat64 = dummy->newFloat64("TestFloat64");
    CmxBool mBool = dummy->newBool("TestBool");
    CmxString mString = dummy->newString("TestString", 123);

    ASSERT_EQ(CmxTypeInfo<CmxTypeTagInt64>::neutral, (CmxTypeInfo<CmxTypeTagInt64>::c_type ) mInt64);
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagFloat64>::neutral, (CmxTypeInfo<CmxTypeTagFloat64>::c_type ) mFloat64);
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagBool>::neutral, (CmxTypeInfo<CmxTypeTagBool>::c_type )mBool);
    ASSERT_EQ(CmxTypeInfo<CmxTypeTagString>::neutral, (CmxTypeInfo<CmxTypeTagString>::c_type )mString);

    dummy->remove(mInt64);
    dummy->remove(mFloat64);
    dummy->remove(mBool);
    dummy->remove(mString);

    ASSERT_EQ(0UL, dummy->getMTime(mInt64));
    ASSERT_EQ(0UL, dummy->getMTime(mFloat64));
    ASSERT_EQ(0UL, dummy->getMTime(mBool));
    ASSERT_EQ(0UL, dummy->getMTime(mString));

    ASSERT_EQ(std::string(""), dummy->getName(mInt64));
    ASSERT_EQ(std::string(""), dummy->getName(mFloat64));
    ASSERT_EQ(std::string(""), dummy->getName(mBool));
    ASSERT_EQ(std::string(""), dummy->getName(mString));

    ASSERT_EQ(std::string(""), dummy->getValueAsString(mInt64));
    ASSERT_EQ(std::string(""), dummy->getValueAsString(mFloat64));
    ASSERT_EQ(std::string(""), dummy->getValueAsString(mBool));
    ASSERT_EQ(std::string(""), dummy->getValueAsString(mString));

    ASSERT_TRUE(dummy->isIgnoreErrors());

    ASSERT_EQ(dummy->begin(), dummy->end());
}
