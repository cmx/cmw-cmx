/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <algorithm>
#include <cmw-cmx-cpp/CmxException.h>
#include <cmw-cmx-cpp/impl/CmxExceptionHelper.h>

extern "C"
{
#include <cmw-cmx/common.h>
}

using namespace std;
using namespace cmw::cmx;

TEST(TestCmxException,helper)
{
    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_COMPONENT_FULL), CmxComponentFullException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_COMPONENT_FULL), CmxComponentFullException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_CONCURRENT_MODIFICATION), CmxConcurrentModificationException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_CONCURRENT_MODIFICATION), CmxConcurrentModificationException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_CORRUPT_SEGMENT), CmxCorruptSegmentException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_CORRUPT_SEGMENT), CmxCorruptSegmentException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_CREATE_FAILED), CmxCreateFailedException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_CREATE_FAILED), CmxCreateFailedException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_INVALID_ARGUMENT), CmxInvalidArgumentException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_INVALID_ARGUMENT), CmxInvalidArgumentException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_INVALID_PID), CmxInvalidPIDException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_INVALID_PID), CmxInvalidPIDException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_INVALID_HANDLE), CmxInvalidHandleException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_INVALID_HANDLE), CmxInvalidHandleException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_INVALID_PID), CmxInvalidPIDException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_INVALID_PID), CmxInvalidPIDException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_NAME_EXISTS), CmxNameExistsException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_NAME_EXISTS), CmxNameExistsException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_NOT_FOUND), CmxNotFoundException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_NOT_FOUND), CmxNotFoundException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_OPERATION_FAILED), CmxOperationFailedException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_OPERATION_FAILED), CmxOperationFailedException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_OUT_OF_MEMORY), CmxOutOfMemoryException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_OUT_OF_MEMORY), CmxOutOfMemoryException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_OUT_OF_RANGE), CmxOutOfRangeException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_OUT_OF_RANGE), CmxOutOfRangeException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_PROTOCOL_VERSION), CmxProtocolVersionException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_PROTOCOL_VERSION), CmxProtocolVersionException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_TYPE_MISMATCH), CmxTypeMismatchException);
    EXPECT_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_TYPE_MISMATCH), CmxTypeMismatchException);

    EXPECT_THROW(CMX_THROW_EXCEPTION(E_CMX_SUCCESS), CmxException);
    EXPECT_NO_THROW(CMX_THROW_EXCEPTION_IF(E_CMX_SUCCESS));
}
