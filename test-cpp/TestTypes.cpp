/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>

#include <cmw-cmx-cpp/Component.h>

extern "C"
{
#include <cmw-cmx/log.h>
#include <cmw-cmx/common.h>
}
using namespace std;
using namespace cmw::cmx;

//-----------------------------------------------------------------------------
static const char * very_long_string = "" //
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !"
                "Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !Hello World !";

//-----------------------------------------------------------------------------

TEST(TestTypes, CmxInt64)
{
    ComponentPtr c = Component::create("Test");
    CmxInt64 m1 = c->newInt64("CmxInt64");
    m1 = __INT_MAX__;
    EXPECT_EQ(__INT_MAX__, (int )m1);

    EXPECT_NO_THROW(cmx_cast<CmxTypeTagInt64>(m1));
    EXPECT_THROW(cmx_cast<CmxTypeTagFloat64>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagBool>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagString>(m1), CmxTypeCastException);
}

TEST(TestTypes, CmxFloat64)
{

    ComponentPtr c = Component::create("Test");
    CmxFloat64 m1 = c->newFloat64("CmxFloat64");
    m1 = 123.123;
    EXPECT_EQ(123.123, (double )m1);

    EXPECT_THROW(cmx_cast<CmxTypeTagInt64>(m1), CmxTypeCastException);
    EXPECT_NO_THROW(cmx_cast<CmxTypeTagFloat64>(m1));
    EXPECT_THROW(cmx_cast<CmxTypeTagBool>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagString>(m1), CmxTypeCastException);
}

TEST(TestTypes, CmxBool)
{
    ComponentPtr c = Component::create("Test");
    CmxBool m1 = c->newBool("CmxBool");
    m1 = true;
    EXPECT_EQ(true, (bool )m1);
    m1 = false;
    EXPECT_FALSE((bool )m1);

    EXPECT_THROW(cmx_cast<CmxTypeTagInt64>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagFloat64>(m1), CmxTypeCastException);
    EXPECT_NO_THROW(cmx_cast<CmxTypeTagBool>(m1));
    EXPECT_THROW(cmx_cast<CmxTypeTagString>(m1), CmxTypeCastException);
}

TEST(TestTypes, CmxString)
{
    ComponentPtr c = Component::create("Test");
    CmxString m1 = c->newString("tooSmall", 10);
    m1 = "";
    EXPECT_EQ("", (std::string )m1);
    m1 = "a";
    EXPECT_EQ("a", (std::string )m1);
    m1 = "1234567890";
    Log::setLogLevel(Log::level::AUDIT);
    EXPECT_THROW(m1 = "1234567890X", CmxInvalidArgumentException);
    EXPECT_THROW(m1 = very_long_string, CmxInvalidArgumentException);
    Log::setLogLevel(-1);
    EXPECT_EQ("1234567890", (std::string )m1);

    EXPECT_THROW(cmx_cast<CmxTypeTagInt64>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagFloat64>(m1), CmxTypeCastException);
    EXPECT_THROW(cmx_cast<CmxTypeTagBool>(m1), CmxTypeCastException);
    EXPECT_NO_THROW(cmx_cast<CmxTypeTagString>(m1));
}

