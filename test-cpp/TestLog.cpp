/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <gtest/gtest.h>
#include <list>
#include <cmw-cmx-cpp/Log.h>

//extern "C"
//{
//#include <cmw-cmx/common.h>
//}

using namespace std;
using namespace cmw::cmx;

TEST(TestLog,position)
{
    Log::Position p = LOG_POSITION();
    EXPECT_EQ((__LINE__ -1), p.line_);
    EXPECT_STREQ(__FILE__, p.source_);
    EXPECT_STREQ(__FUNCTION__, p.function_);
}

TEST(TestLog, set_get_loglevel)
{
    int log_level = Log::getLogLevel();
    Log::setLogLevel(Log::level::AUDIT);
    EXPECT_EQ(Log::level::AUDIT, Log::getLogLevel());
    Log::setLogLevel(log_level);
}

struct LogInfos
{
    int log_level;
    std::string message;
    bool truncated;
};

static std::list<LogInfos> logMessages;

static void log_adapter(int log_level, const char * message, bool truncated)
{
    LogInfos i;
    i.log_level = log_level;
    i.message = message;
    i.truncated = truncated;
    logMessages.push_back(i);
}

TEST(TestLog, logging)
{
    logMessages.clear();
    Log::setLogAdapter(log_adapter);
    Log::trace(LOG_POSITION(), "Test trace %d", 1);
    EXPECT_EQ(Log::level::TRACE, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test trace 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::debug(LOG_POSITION(), "Test debug %d", 1);
    EXPECT_EQ(Log::level::DEBUG, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test debug 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::info(LOG_POSITION(), "Test info %d", 1);
    EXPECT_EQ(Log::level::INFO, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test info 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::warn(LOG_POSITION(), "Test warn %d", 1);
    EXPECT_EQ(Log::level::WARN, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test warn 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::error(LOG_POSITION(), "Test error %d", 1);
    EXPECT_EQ(Log::level::ERROR, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test error 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::audit(LOG_POSITION(), "Test audit %d", 1);
    EXPECT_EQ(Log::level::AUDIT, logMessages.back().log_level);
    EXPECT_TRUE(logMessages.back().message.find("Test audit 1") != std::string::npos);
    EXPECT_FALSE(logMessages.back().truncated);

    Log::setLogAdapter(NULL);
}
