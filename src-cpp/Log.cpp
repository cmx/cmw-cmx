/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/Log.h>

extern "C"
{
#include <cmw-cmx/log.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/atomic.h>
}

namespace cmw
{
namespace cmx
{
namespace Log
{

static log_adapter_t log_cpp_adapter = NULL;

static void cpp_log_adapter(enum cmx_log_levels level, const char * source_file, int line, const char * function,
        const char * fmt, va_list ap)
{
    char message_buf[256];
    int c = snprintf(message_buf, 256, "%s:%d:%s ", source_file, line, function);
    c = vsnprintf(message_buf + c, 256 - c, fmt, ap);
    log_cpp_adapter((int) level, message_buf, c == 256);
}

const int level::AUDIT = LOG_LEVEL_AUDIT;
const int level::ERROR = LOG_LEVEL_ERROR;
const int level::WARN = LOG_LEVEL_WARN;
const int level::INFO = LOG_LEVEL_INFO;
const int level::DEBUG = LOG_LEVEL_DEBUG;
const int level::TRACE = LOG_LEVEL_TRACE;

void setLogAdapter(log_adapter_t adapter_func)
{
    log_cpp_adapter = adapter_func;
    if (adapter_func != NULL)
    {
        cmx_atomic_smp_mb();
        cmx_log_set_adapter(cpp_log_adapter);
    }
    else
    {
        cmx_log_set_adapter(NULL);
    }
}

void setLogLevel(const int level) {
    cmx_log_current_level = level;
}

int getLogLevel() {
    return cmx_log_current_level;
}

void audit(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_AUDIT, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}
void error(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_ERROR, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}
void warn(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_WARN, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}
void info(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_INFO, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}
void debug(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_DEBUG, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}
void trace(const Position & p, const char * fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(LOG_LEVEL_TRACE, p.source_, p.line_, p.function_, fmt, args);
    va_end(args);
}

} // namespace Log
} // namespace cmx
} // namespace cmw
