/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cstdlib>

#include <cmw-cmx-cpp/demo/Demo2.h>

#include <cmw-cmx-cpp/CmxSupport.h>
#include <cmw-cmx-cpp/ProcessComponent.h>

namespace cmw {
namespace cmx {
namespace demo {

using namespace cmw::cmx;

class Demo2Metrics : CmxSupport
{
public:
    Demo2Metrics(const std::string & name) :
                    random(newInt64("Demo2Metrics." + name, "random"))
    {
    }
    CmxInt64 random;
};


Demo2::Demo2(const std::string & name)
{
    metrics = new Demo2Metrics(name);
}

Demo2::~Demo2()
{
    delete metrics;
}

void Demo2::execute()
{
    metrics->random = std::rand();
}

void Demo2::updateProcess() {
    ProcessComponent::update();
}
} // namespace demo
} // namespace cmx
} // namespace cmw
