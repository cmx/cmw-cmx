/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/ProcessComponent.h>

extern "C"
{
#include <cmw-cmx/process.h>
}

namespace cmw
{
namespace cmx
{
void ProcessComponent::update()
{
    cmx_process_update();
}
} // namespace cmx
} // namespace cmw
