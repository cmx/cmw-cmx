#include <cmw-cmx-cpp/CmxException.h>
extern "C"
{
#include <cmw-cmx/common.h>
}
namespace cmw
{
namespace cmx
{

CmxErrorCodeException::CmxErrorCodeException(int errorCode) :
                CmxException(cmx_common_strerror(errorCode)),
                errorCode_(errorCode)
{
}

CmxOperationFailedException::CmxOperationFailedException() :
                CmxErrorCodeException(E_CMX_OPERATION_FAILED)
{
}

CmxInvalidHandleException::CmxInvalidHandleException() :
                CmxErrorCodeException(E_CMX_INVALID_HANDLE)
{
}

CmxTypeMismatchException::CmxTypeMismatchException() :
                CmxErrorCodeException(E_CMX_TYPE_MISMATCH)
{
}

CmxCorruptSegmentException::CmxCorruptSegmentException() :
                CmxErrorCodeException(E_CMX_CORRUPT_SEGMENT)
{
}

CmxInvalidArgumentException::CmxInvalidArgumentException() :
                CmxErrorCodeException(E_CMX_INVALID_ARGUMENT)
{
}

CmxOutOfMemoryException::CmxOutOfMemoryException() :
                CmxErrorCodeException(E_CMX_OUT_OF_MEMORY)
{
}

CmxCreateFailedException::CmxCreateFailedException() :
                CmxErrorCodeException(E_CMX_CREATE_FAILED)
{
}

CmxInvalidPIDException::CmxInvalidPIDException() :
                CmxErrorCodeException(E_CMX_INVALID_PID)
{
}

CmxConcurrentModificationException::CmxConcurrentModificationException() :
                CmxErrorCodeException(E_CMX_CONCURRENT_MODIFICATION)
{
}

CmxNameExistsException::CmxNameExistsException() :
                CmxErrorCodeException(E_CMX_NAME_EXISTS)
{
}

CmxNotFoundException::CmxNotFoundException() :
                CmxErrorCodeException(E_CMX_NOT_FOUND)
{
}

CmxComponentFullException::CmxComponentFullException() :
                CmxErrorCodeException(E_CMX_COMPONENT_FULL)
{
}

CmxProtocolVersionException::CmxProtocolVersionException() :
                CmxErrorCodeException(E_CMX_PROTOCOL_VERSION)
{
}

CmxOutOfRangeException::CmxOutOfRangeException() :
                CmxErrorCodeException(E_CMX_OUT_OF_RANGE)
{
}

} // namespace cmx
} // namespace cmw
