/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/Registry.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/ImmutableComponent.h>
#include <cmw-cmx-cpp/Log.h>
#include <cmw-cmx-cpp/impl/CmxExceptionHelper.h>

extern "C"
{
#include <unistd.h>
#include <cmw-cmx/shm.h>
#include <cmw-cmx/registry.h>
}

namespace cmw
{
namespace cmx
{

/*
 * This class is implementation detail.
 * It provides a public but not public-api factory method.
 */
class RegistryCreatedImmutableComponent : public ImmutableComponent
{
public:
    static ImmutableComponentPtr open(cmx_shm * cmx_shm_ptr)
    {
        return ImmutableComponent::open(cmx_shm_ptr);
    }
};

struct ComponentInfo
{
    ComponentInfo(size_t l) :
                    len(l),
                    process_ids(l),
                    names(l),
                    tags(l)
    {
    }
    size_t len;
    std::vector<int> process_ids;
    std::vector<std::string> names;
    std::vector<std::string> tags;
};

// ComponentPredicate
ComponentPredicate::result_type ComponentPredicate::operator()(
        ComponentPredicate::argument_type immutableComponent) const
{
    bool r = true;
    if (matchName_)
    {
        r = r and (immutableComponent->name() == name_);
    }
    if (matchProcessId_)
    {
        r = r and (immutableComponent->processId() == processId_);
    }
    Log::trace(LOG_POSITION(), "r=%d matchName_=%d name_%s => %s matchProcessId_=%d processId_=%d => =%d", (int) r,
            (int) matchName_, name_.c_str(), immutableComponent->name().c_str(), (int) matchProcessId_, processId_,
            immutableComponent->processId());
    return r;
}

// ComponentIterator
RegistryIterator::RegistryIterator() :
                position_(0)
{
    componentInfo_ = RegistryIterator::discover();
    if (not componentInfo_ or position_ >= componentInfo_->len)
    {
        position_ = __INT_MAX__;
    }
}

RegistryIterator::value_type RegistryIterator::operator *()
{
    std::string & name = componentInfo_->names.at(position_);
    int process_id = componentInfo_->process_ids.at(position_);
    cmx_shm * cmx_shm_ptr;
    int r = cmx_shm_open_ro(process_id, name.c_str(), &cmx_shm_ptr);
    CMX_THROW_EXCEPTION_IF(r);
    return RegistryCreatedImmutableComponent::open(cmx_shm_ptr);
}

RegistryIterator & RegistryIterator::operator ++()
{
    position_++;
    if (not componentInfo_ or position_ >= componentInfo_->len or position_ < 0)
    {
        position_ = __INT_MAX__;
    }
    return *this;
}

RegistryIterator RegistryIterator::operator ++(int)
{
    RegistryIterator tmpCopy = (*this);
    this->operator ++();
    return tmpCopy;
}

std::string RegistryIterator::name()
{
    return componentInfo_->names.at(position_);
}

int RegistryIterator::processId()
{
    return componentInfo_->process_ids.at(position_);
}

RegistryIterator RegistryIterator::end()
{
    return RegistryIterator(__INT_MAX__, ComponentInfoPtr());
}

ComponentInfoPtr RegistryIterator::discover()
{
    int l;
    cmx_component_info * component_info;
    if (E_CMX_SUCCESS == cmx_registry_discover(&component_info, &l))
    {
        ComponentInfo * ci = new ComponentInfo(l);
        for (int i = 0; i < l; i++)
        {
            ci->names[i] = component_info[i].name;
            ci->process_ids[i] = component_info[i].process_id;
            ci->tags[i] = std::string(component_info[i].CMX_TAG, 8);
        }
        free(component_info);
        return ComponentInfoPtr(ci);
    }
    else
    {
        return ComponentInfoPtr(new ComponentInfo(0U));
    }
}

// Registry
ImmutableComponentPtr Registry::open(const int processId, const std::string & componentName)
{
    cmx_shm * cmx_shm_ptr;
    int r = cmx_shm_open_ro(processId, componentName.c_str(), &cmx_shm_ptr);
    CMX_THROW_EXCEPTION_IF(r);
    return RegistryCreatedImmutableComponent::open(cmx_shm_ptr);
}

int Registry::count()
{
    int count = 0;
    cmx_registry_discover(NULL, &count);
    return count;
}

void Registry::cleanup()
{
    cmx_registry_cleanup();
}

} // namespace cmx
} // namespace cmw
