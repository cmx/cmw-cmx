/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/impl/CmxSupportImpl.h>

namespace cmw
{
namespace cmx
{
namespace impl
{

CmxSupportImpl::CmxSupportImpl(bool ignoreError) :
                ignoreError_(ignoreError)
{
    pthread_mutex_init(&componentsLock, NULL);
}

ComponentPtr CmxSupportImpl::getComponent(const std::string & name)
{
    if (pthread_mutex_lock(&componentsLock) != 0) throw CmxTypeMismatchException();

    if (components.find(name) != components.end())
    {
        ComponentPtr p = components.find(name)->second;
        pthread_mutex_unlock(&componentsLock);
        return p;
    }
    else
    {
        // create
        ComponentPtr p = Component::create(name, ignoreError_);
        components.insert(components.begin(), std::make_pair(name, p));
        pthread_mutex_unlock(&componentsLock);
        return p;
    }
}

CmxInt64 CmxSupportImpl::newInt64(const std::string & component_name, const std::string & value_name)
{
    ComponentPtr p = getComponent(component_name);
    return p->newInt64(value_name);
}

CmxFloat64 CmxSupportImpl::newFloat64(const std::string & component_name, const std::string & value_name)
{
    ComponentPtr p = getComponent(component_name);
    return p->newFloat64(value_name);
}

CmxBool CmxSupportImpl::newBool(const std::string & component_name, const std::string & value_name)
{
    ComponentPtr p = getComponent(component_name);
    return p->newBool(value_name);
}

CmxString CmxSupportImpl::newString(const std::string & component_name, const std::string & value_name, const uint16_t size)
{
    ComponentPtr p = getComponent(component_name);
    return p->newString(value_name, size);
}

} // namespace impl
} // namespace cmx
} // namespace cmw
