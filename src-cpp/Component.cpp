/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
extern "C"
{
#include <unistd.h>
#include <cmw-cmx/shm.h>
#include <cmw-cmx/registry.h>
}
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/CmxException.h>
#include <cmw-cmx-cpp/impl/CmxExceptionHelper.h>

namespace cmw
{
namespace cmx
{

template<typename _CmxTypeTag>
static typename CmxTypeInfo<_CmxTypeTag>::mutable_type newHelper(cmx_shm * cmx_shm_ptr_, bool ignoreErrors,
        ComponentPtr p, const std::string & name)
{
    typedef typename CmxTypeInfo<_CmxTypeTag>::mutable_type Cmx_Var_Type;

    int value_handle = cmx_shm_add_value_single(cmx_shm_ptr_, _CmxTypeTag::cmx_type, name.c_str());
    if (value_handle < 0)
    {
        // handle error:
        if (ignoreErrors)
        {
            return Cmx_Var_Type(p); // return unitialized ref
        }
        else
        {
            CMX_THROW_EXCEPTION(value_handle);
            return Cmx_Var_Type(p); // never reached
        }
    }
    else
    {
        return Cmx_Var_Type(p, value_handle);
    }
}

CmxInt64 Component::newInt64(const std::string & name)
{
    if (isDummyMode()) return CmxInt64(ComponentPtr(weakSelf));
    return newHelper<CmxTypeTagInt64>((cmx_shm*) cmx_shm_ptr_, ignoreErrors_, ComponentPtr(weakSelf), name);
}

CmxFloat64 Component::newFloat64(const std::string & name)
{
    if (isDummyMode()) return CmxFloat64(ComponentPtr(weakSelf));
    return newHelper<CmxTypeTagFloat64>((cmx_shm*) cmx_shm_ptr_, ignoreErrors_, ComponentPtr(weakSelf), name);
}

CmxBool Component::newBool(const std::string & name)
{
    if (isDummyMode()) return CmxBool(ComponentPtr(weakSelf));
    return newHelper<CmxTypeTagBool>((cmx_shm*) cmx_shm_ptr_, ignoreErrors_, ComponentPtr(weakSelf), name);
}

CmxString Component::newString(const std::string & name, const uint16_t size)
{
    if (isDummyMode()) return CmxString(ComponentPtr(weakSelf));
    int value_handle = cmx_shm_add_value_string((cmx_shm*) cmx_shm_ptr_, name.c_str(), size);
    if (value_handle < 0)
    {
        // handle error:
        if (ignoreErrors_)
        {
            return CmxString(ComponentPtr(weakSelf)); // return unitialized ref
        }
        else
        {
            CMX_THROW_EXCEPTION(value_handle);
            return CmxString(ComponentPtr(weakSelf)); // never reached
        }
    }
    else
    {
        return CmxString(ComponentPtr(weakSelf), value_handle);
    }
}

void Component::set(const CmxInt64 & ref, const CmxInt64::c_type & value)
{
    if (dummyMode_) return;
    cmx_shm_value shm_value;
    shm_value._int64 = value;
    int returncode = cmx_shm_set_value_single((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, CmxTypeTagInt64::cmx_type,
            &shm_value);
    if (not ignoreErrors_) CMX_THROW_EXCEPTION_IF(returncode);
}

void Component::set(const CmxFloat64 & ref, const CmxFloat64::c_type & value)
{
    if (dummyMode_) return;
    cmx_shm_value shm_value;
    shm_value._float64 = value;
    int returncode = cmx_shm_set_value_single((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, CmxTypeTagFloat64::cmx_type,
            &shm_value);
    if (not ignoreErrors_) CMX_THROW_EXCEPTION_IF(returncode);
}

void Component::set(const CmxBool & ref, const CmxBool::c_type & value)
{
    if (dummyMode_) return;
    cmx_shm_value shm_value;
    shm_value._bool = value;
    int returncode = cmx_shm_set_value_single((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, CmxTypeTagBool::cmx_type,
            &shm_value);
    if (not ignoreErrors_) CMX_THROW_EXCEPTION_IF(returncode);
}

void Component::set(const CmxString & ref, const CmxString::c_type & value)
{
    if (dummyMode_) return;
    if (value.size() > (size_t) __INT_MAX__) throw CmxInvalidArgumentException();
    int returncode = cmx_shm_set_value_string((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, value.c_str(),
            (int) value.size());
    if (not ignoreErrors_) CMX_THROW_EXCEPTION_IF(returncode);
}

void Component::remove(const CmxRef & ref)
{
    if (dummyMode_) return;
    int returncode = cmx_shm_remove_value((cmx_shm*) cmx_shm_ptr_, ref.value_handle_);
    if (not ignoreErrors_) CMX_THROW_EXCEPTION_IF(returncode);
}

// static
ComponentPtr Component::create(const std::string name, bool ignoreError)
{
    cmx_shm * cmx_shm_ptr;
    int r = cmx_shm_create(name.c_str(), &cmx_shm_ptr);
    if (r < 0)
    {
        if (ignoreError)
        {
            // return dummy component
            return Component::dummy();
        }
        else
        {
            CMX_THROW_EXCEPTION(r);
            throw CmxCreateFailedException();
        }
    }
    else
    {
        Component * instance = new Component(cmx_shm_ptr);
        instance->setIgnoreErrors(ignoreError);
        ComponentPtr pic(instance);
        instance->weakSelf = pic;
        instance->weakImmutableSelf = pic;
        return pic;
    }
}

//static
ComponentPtr Component::dummy()
{
    // return dummy component
    Component * instance = new Component();
    instance->setIgnoreErrors(true);
    ComponentPtr pic(instance);
    instance->weakSelf = pic;
    instance->weakImmutableSelf = pic;
    return pic;
}

Component::~Component()
{
    if (cmx_shm_ptr_ != NULL)
    {
        Log::trace(LOG_POSITION(), "Component destroy ptr=%p", cmx_shm_ptr_);
        cmx_shm_destroy((cmx_shm*) cmx_shm_ptr_);
        cmx_shm_ptr_ = NULL;
    }
    else
    {
        Log::trace(LOG_POSITION(), "Component is unmapped ptr=%p", cmx_shm_ptr_);
    }
}

Component::Component() :
                ImmutableComponent(NULL),
                dummy_(true)
{
    Log::trace(LOG_POSITION(), "Component is created ptr=%p dummy", cmx_shm_ptr_);
}

Component::Component(void * cmx_shm_ptr) :
                ImmutableComponent(cmx_shm_ptr),
                dummy_(false)
{
    Log::trace(LOG_POSITION(), "New component ptr=%p", cmx_shm_ptr_);
}

} // namespace cmx
} // namespace cmw
