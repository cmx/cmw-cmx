/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <sstream>
#include <cmw-cmx-cpp/ImmutableComponent.h>
#include <cmw-cmx-cpp/Log.h>
#include <cmw-cmx-cpp/impl/CmxExceptionHelper.h>

extern "C"
{
#include <unistd.h>
#include <cmw-cmx/shm.h>
#define CMW_CMX_USE_PRIVATE
#include <cmw-cmx/shm-private.h>
#include <sched.h>
}

namespace cmw
{
namespace cmx
{

// Metric_iterator
void ComponentIterator::init()
{
    // do not initialise end iterator
    if (pos_ == ComponentIterator::end) return;

    int result = cmx_shm_find_value((cmx_shm*) immutableComponent_->cmx_shm_ptr_, pos_, &value_handle_, &value_type_);
    if (result == E_CMX_SUCCESS)
    {
        pos_ = cmx_shm_unpack_index(value_handle_);
    }
    else
    {
        pos_ = ComponentIterator::end;
        value_handle_ = -1;
        value_type_ = -1;
    }
}

CmxRef ComponentIterator::operator*()
{
    return CmxRef(value_type_, immutableComponent_, value_handle_);
}

ComponentIterator ComponentIterator::operator++()
{
    if (pos_ == ComponentIterator::end)
    {
        return *this;
    }
    pos_++;
    int result = cmx_shm_find_value((cmx_shm*) immutableComponent_->cmx_shm_ptr_, pos_, &value_handle_, &value_type_);
    if (result == E_CMX_SUCCESS)
    {
        pos_ = cmx_shm_unpack_index(value_handle_);
    }
    else
    {
        pos_ = ComponentIterator::end;
        value_handle_ = -1;
        value_type_ = -1;
    }
    return *this;
}

ComponentIterator ComponentIterator::operator++(int)
{
    ComponentIterator tmpCopy = *this;
    this->operator ++();
    return tmpCopy;
}

///////////////

template<typename _CmxTypeTag>
static typename CmxTypeInfo<_CmxTypeTag>::c_type getSingleHelper(cmx_shm * cmx_shm_ptr, int value_handle,
        int ignoreErrors, int readRetry)
{
    int shm_type;
    cmx_shm_value shm_value;
    int r;

    if (readRetry == 1)
    {
        r = cmx_shm_get_value_single(cmx_shm_ptr, value_handle, &shm_type, &shm_value, NULL);
    }
    else
    {
        for (int round = readRetry; round != 0 or readRetry == 0; round--)
        {
            r = cmx_shm_get_value_single(cmx_shm_ptr, value_handle, &shm_type, &shm_value, NULL);
            if (r != E_CMX_CONCURRENT_MODIFICATION)
            {
                break;
            }
            sched_yield();
        }
    }

    if (r == E_CMX_SUCCESS)
    {
        if (shm_type != _CmxTypeTag::cmx_type)
        {
            // returncode r success but type mismatch
            if (ignoreErrors)
            {
                return CmxTypeInfo<_CmxTypeTag>::neutral;
            }
            else
            {
                throw CmxTypeCastException(shm_type, _CmxTypeTag::cmx_type);
            }
        }
        else
        {
            // returncode success and type matches
            switch (_CmxTypeTag::cmx_type)
            { // replace casts with selector tempalte
                case CMX_TYPE_INT64:
                    return (typename CmxTypeInfo<_CmxTypeTag>::c_type) shm_value._int64;
                case CMX_TYPE_FLOAT64:
                    return (typename CmxTypeInfo<_CmxTypeTag>::c_type) shm_value._float64;
                case CMX_TYPE_BOOL:
                    return (typename CmxTypeInfo<_CmxTypeTag>::c_type) shm_value._bool;
                default:
                    return CmxTypeInfo<_CmxTypeTag>::neutral;
            }
        }
    }
    else
    {
        // returncode != success
        if (ignoreErrors)
        {
            // return neutral
            return CmxTypeInfo<_CmxTypeTag>::neutral;
        }
        else
        {
            CMX_THROW_EXCEPTION(r);
            return CmxTypeInfo<_CmxTypeTag>::neutral; // never reached
        }
    }
}

CmxImmutableInt64::c_type ImmutableComponent::getValue(const CmxImmutableInt64 & ref)
{
    if (dummyMode_) return CmxTypeInfo<CmxTypeTagInt64>::neutral;
    return getSingleHelper<CmxTypeTagInt64>((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, ignoreErrors_, readRetry_);
}

CmxImmutableFloat64::c_type ImmutableComponent::getValue(const CmxImmutableFloat64 & ref)
{
    if (dummyMode_) return CmxTypeInfo<CmxTypeTagFloat64>::neutral;
    return getSingleHelper<CmxTypeTagFloat64>((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, ignoreErrors_, readRetry_);
}

CmxImmutableBool::c_type ImmutableComponent::getValue(const CmxImmutableBool & ref)
{
    if (dummyMode_) return CmxTypeInfo<CmxTypeTagBool>::neutral;
    return getSingleHelper<CmxTypeTagBool>((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, ignoreErrors_, readRetry_);
}

static void cmx_shm_get_value_callback(const char * data, size_t size, void * arg)
{
    std::string * value = reinterpret_cast<std::string*>(arg);
    value->append(data);
}

CmxImmutableString::c_type ImmutableComponent::getValue(const CmxImmutableString & ref)
{
    if (dummyMode_) return CmxTypeInfo<CmxTypeTagString>::neutral;

    std::string value;
    int r;

    if (readRetry_ == 1)
    {
        r = cmx_shm_get_value_string((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, cmx_shm_get_value_callback,
                (void*) &value, NULL);
    }
    else
    {
        for (int round = readRetry_; round != 0 or readRetry_ == 0; round--)
        {
            value.clear();
            r = cmx_shm_get_value_string((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, cmx_shm_get_value_callback,
                    (void*) &value, NULL);
            if (r != E_CMX_CONCURRENT_MODIFICATION)
            {
                break;
            }
            sched_yield();
        }
    }

    if (r == E_CMX_SUCCESS)
    {
        return value;
    }
    else
    {
        // returncode != success
        if (ignoreErrors_)
        {
            // return neutral
            return CmxTypeInfo<CmxTypeTagString>::neutral;
        }
        else
        {
            CMX_THROW_EXCEPTION(r);
            return CmxTypeInfo<CmxTypeTagString>::neutral; // never reached
        }
    }
}

uint64_t ImmutableComponent::getMTime(const CmxRef & ref)
{
    if (dummyMode_) return 0;
    cmx_shm_value value;
    uint64_t value_mtime;
    int value_type;
    int r;

    if (readRetry_ == 1)
    {
        r = cmx_shm_get_value_single((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, &value_type, &value, &value_mtime);
    }
    else
    {
        for (int round = readRetry_; round != 0 or readRetry_ == 0; round--)
        {
            r = cmx_shm_get_value_single((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, &value_type, &value, &value_mtime);
            if (r != E_CMX_CONCURRENT_MODIFICATION)
            {
                break;
            }
            sched_yield();
        }
    }

    if (r == E_CMX_SUCCESS)
    {
        return value_mtime;
    }
    else
    {
        if (ignoreErrors_)
        {
            return 0;
        }
        else
        {
            CMX_THROW_EXCEPTION(r);
            return 0; // never reached
        }
    }
}

std::string ImmutableComponent::getValueAsString(const CmxRef & ref)
{
    if (dummyMode_) return "";
    std::ostringstream os;
    switch (ref.type_)
    {
        case CMX_TYPE_INT64:
            os << this->getValue(CmxImmutableInt64(ref.immutableComponent_, ref.value_handle_));
            break;
        case CMX_TYPE_FLOAT64:
            os << this->getValue(CmxImmutableFloat64(ref.immutableComponent_, ref.value_handle_));
            break;
        case CMX_TYPE_BOOL:
            os << this->getValue(CmxImmutableBool(ref.immutableComponent_, ref.value_handle_));
            break;
        case CMX_TYPE_STRING:
            os << this->getValue(CmxImmutableString(ref.immutableComponent_, ref.value_handle_));
            break;
        default:
            Log::warn(LOG_POSITION(), "Unknown type code=%d", ref.type_);
            os << "";
            break;
    }
    return os.str();
}

std::string ImmutableComponent::getName(const CmxRef & ref)
{
    if (dummyMode_) return "";
    std::string value;
    char buf[CMX_NAME_SZ];

    int r = cmx_shm_get_value_name((cmx_shm*) cmx_shm_ptr_, ref.value_handle_, buf);
    if (r == E_CMX_SUCCESS)
    {
        value.append(buf);
        return value;
    }
    else
    {
        CMX_THROW_EXCEPTION(r);
        return value; // never reached
    }
}

std::string ImmutableComponent::name() const
{
    if (dummyMode_) return "";
    cmx_shm * cmx = (cmx_shm*) cmx_shm_ptr_;
    return std::string(cmx->name);
}

int ImmutableComponent::processId() const
{
    if (dummyMode_) return 0;
    cmx_shm * cmx = (cmx_shm*) cmx_shm_ptr_;
    return cmx->process_id;
}

ImmutableComponent::iterator ImmutableComponent::begin()
{
    if (dummyMode_) return iterator(ImmutableComponentPtr(weakImmutableSelf), iterator::end);
    else return iterator(ImmutableComponentPtr(weakImmutableSelf), 0);
}

ImmutableComponent::iterator ImmutableComponent::end()
{
    return iterator(ImmutableComponentPtr(weakImmutableSelf), iterator::end);
}

bool ImmutableComponent::isDummyMode()
{
    return dummyMode_;
}

bool ImmutableComponent::isIgnoreErrors()
{
    return ignoreErrors_;
}

void ImmutableComponent::setIgnoreErrors(const bool ignoreErrors)
{
    ignoreErrors_ = ignoreErrors;
}

int ImmutableComponent::getReadRetry()
{
    return readRetry_;
}

void ImmutableComponent::setReadRetry(const int readRetry)
{
    readRetry_ = readRetry;
}

ImmutableComponent::~ImmutableComponent()
{
    if (cmx_shm_ptr_ != NULL)
    {
        Log::trace(LOG_POSITION(), "ImmutableComponent munmap ptr=%p", cmx_shm_ptr_);
        cmx_shm_unmap((cmx_shm*) cmx_shm_ptr_);
        cmx_shm_ptr_ = NULL;
    }
    else
    {
        Log::trace(LOG_POSITION(), "ImmutableComponent is unmapped ptr=%p", cmx_shm_ptr_);
    }
}

ImmutableComponentPtr ImmutableComponent::open(void * cmx_shm_ptr)
{
    ImmutableComponentPtr pic(new ImmutableComponent(cmx_shm_ptr));
    pic->weakImmutableSelf = pic;
    return pic;
}

ImmutableComponent::ImmutableComponent(void * cmx_shm_ptr) :
                cmx_shm_ptr_(cmx_shm_ptr),
                ignoreErrors_(false),
                readRetry_(20)
{
    dummyMode_ = (cmx_shm_ptr == NULL);
    Log::trace(LOG_POSITION(), "New ImmutableComponent ptr=%p", cmx_shm_ptr_);
}
} // namespace cmx
} // namespace cmw
