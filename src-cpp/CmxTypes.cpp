/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <cmw-cmx-cpp/CmxTypes.h>
#include <cmw-cmx-cpp/Component.h>
#include <cmw-cmx-cpp/impl/CmxExceptionHelper.h>

extern "C"
{
#include <cmw-cmx/shm.h>
}

namespace cmw
{
namespace cmx
{

const int CmxTypeTagInt64::cmx_type = CMX_TYPE_INT64;
const int CmxTypeTagFloat64::cmx_type = CMX_TYPE_FLOAT64;
const int CmxTypeTagBool::cmx_type = CMX_TYPE_BOOL;
const int CmxTypeTagString::cmx_type = CMX_TYPE_STRING;

const CmxTypeInfo<CmxTypeTagInt64>::c_type CmxTypeInfo<CmxTypeTagInt64>::neutral = 0L;
const CmxTypeInfo<CmxTypeTagFloat64>::c_type CmxTypeInfo<CmxTypeTagFloat64>::neutral = 0.0f;
const CmxTypeInfo<CmxTypeTagBool>::c_type CmxTypeInfo<CmxTypeTagBool>::neutral = false;
const CmxTypeInfo<CmxTypeTagString>::c_type CmxTypeInfo<CmxTypeTagString>::neutral = "";

// downcast of ComponentPtr to ImmutableComponentPtr
ImmutableComponentPtr CmxRef::pointerConvert(ComponentPtr p)
{
    return ImmutableComponentPtr(p);
}

uint64_t CmxRef::mtime() const
{
    return immutableComponent_->getMTime(*this);
}

std::string CmxRef::name() const
{
    return immutableComponent_->getName(*this);
}

const std::string CmxRef::type_name() const
{
    switch (type_)
    {
        case CMX_TYPE_INT64:
            return "INT64";
        case CMX_TYPE_BOOL:
            return "BOOL";
        case CMX_TYPE_FLOAT64:
            return "FLOAT64";
        case CMX_TYPE_STRING:
            return "STRING";
        default:
            return "";
    }
}

CmxImmutableInt64::operator CmxImmutableInt64::c_type()
{
    return immutableComponent_->getValue(*this);
}

CmxImmutableFloat64::operator CmxImmutableFloat64::c_type()
{
    return immutableComponent_->getValue(*this);
}

CmxImmutableBool::operator CmxImmutableBool::c_type()
{
    return immutableComponent_->getValue(*this);
}

CmxImmutableString::operator CmxImmutableString::c_type()
{
    return immutableComponent_->getValue(*this);
}

CmxInt64::c_type CmxInt64::operator =(const CmxInt64::c_type & value)
{
    component_->set(*this, value);
    return value;
}

CmxFloat64::c_type CmxFloat64::operator =(const CmxFloat64::c_type & value)
{
    component_->set(*this, value);
    return value;
}

CmxBool::c_type CmxBool::operator =(const CmxBool::c_type & value)
{
    component_->set(*this, value);
    return value;
}

CmxString::c_type CmxString::operator =(const CmxString::c_type & value)
{
    component_->set(*this, value);
    return value;
}

} // namespace cmx
} // namespace cmw
