/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include "cmw-cmx/log.h"
#include "cmw-cmx/common.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int cmx_log_current_level = -1;

static cmx_log_adapter_t _log_adapter = NULL;

void cmx_log_set_adapter(cmx_log_adapter_t log_adapter_func)
{
    _log_adapter = log_adapter_func;
}

const char *cmx_log_level_name(enum cmx_log_levels level)
{
    switch (level)
    {
        case LOG_LEVEL_AUDIT:
            return "AUDIT";
        case LOG_LEVEL_ERROR:
            return "ERROR";
        case LOG_LEVEL_WARN:
            return "WARN";
        case LOG_LEVEL_INFO:
            return "INFO";
        case LOG_LEVEL_DEBUG:
            return "DEBUG";
        case LOG_LEVEL_TRACE:
            return "TRACE";
    }
    return "";
}

enum cmx_log_levels cmx_log_from_string(const char *level)
{
    if (level == NULL)
    {
        // Default log level
        return LOG_LEVEL_INFO;
    }
    else if (strcmp("AUDIT", level) == 0)
    {
        return LOG_LEVEL_AUDIT;
    }
    else if (strcmp("ERROR", level) == 0)
    {
        return LOG_LEVEL_ERROR;
    }
    else if (strcmp("WARN", level) == 0)
    {
        return LOG_LEVEL_WARN;
    }
    else if (strcmp("INFO", level) == 0)
    {
        return LOG_LEVEL_INFO;
    }
    else if (strcmp("DEBUG", level) == 0)
    {
        return LOG_LEVEL_DEBUG;
    }
    else if (strcmp("TRACE", level) == 0)
    {
        return LOG_LEVEL_TRACE;
    }
    // invalid log level name
    fprintf(stdout, "Invalid Log level: %s. Set level to %s\n", level, cmx_log_level_name(LOG_LEVEL_INFO));
    return LOG_LEVEL_INFO;
}

enum cmx_log_levels cmx_log_level_from_env(void)
{
    const char * env_level = getenv("CMX_LOG_LEVEL");
    return cmx_log_from_string(env_level);
}

void cmx_log_vprintf(enum cmx_log_levels level, const char * file, const int line, const char * function, const char * fmt,
        va_list ap)
{
    cmx_log_adapter_t log_adapter = _log_adapter;
    if (log_adapter != NULL)
    {
        // Call a external adapter
        log_adapter(level, file, line, function, fmt, ap);
    }
    else
    {
        if (cmx_log_current_level == -1)
        {
            // if log level is not defined try to parse
            // environment variable or fall back to default
            cmx_log_current_level = cmx_log_level_from_env();
        }
        if (cmx_log_current_level >= (int) level)
        {
            // simple logging to stdout
            printf("[%d]:%s:%s:%d:%s: ", cmx_common_current_pid(), cmx_log_level_name(level), file, line, function);
            vprintf(fmt, ap);
            printf("\n");
        }
    }
}
void cmx_log_printf(enum cmx_log_levels level, const char * file, const int line, const char * function, const char * fmt,
        ...)
{
    va_list args;
    va_start(args, fmt);
    cmx_log_vprintf(level, file, line, function, fmt, args);
    va_end(args);
}
