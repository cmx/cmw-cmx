/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "cmw-cmx/log.h"
#include "cmw-cmx/registry.h"
#include "cmw-cmx/common.h"

#define CMW_CMX_USE_PRIVATE
#include "cmw-cmx/atomic.h"

#define CMW_CMX_USE_PRIVATE
#include "cmw-cmx/shm-private.h"

#define _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, value_index) \
    if (cmx_shm_ptr == NULL) { \
        LOG_ERROR_IF("Argument 'cmx_shm_ptr' is NULL"); \
        return E_CMX_INVALID_ARGUMENT; \
    } \
    if (!(value_index < CMX_NUMBER_OF_VALUES && value_index >= 0)) { \
        LOG_ERROR_IF("Invalid value_index %d", value_index); \
        return E_CMX_INVALID_ARGUMENT; \
    }

/*
 * \brief Increment uint16_t value.
 * This function is non-atomic.
 */
static inline void _next_id(uint16_t * id)
{
    (*id)++;
    // skip 0
    if (*id == 0) (*id)++;
    // skip negative values in resulting handle
    // While the id itself cannot be negative (unsigned 16bit) it is
    // with the index to the value index. There negative values are used to indicate errors.
    if ((*id) & 0x8000) *id = 1;
}

int cmx_shm_unmap(cmx_shm * const cmx_shm_ptr)
{
    LOG_DEBUG_IF("Unmap CMX shared memory object ptr=%p component_name=%s", (void* )cmx_shm_ptr, cmx_shm_ptr->name);
    if (munmap(cmx_shm_ptr, sizeof(cmx_shm)) == 0)
    {
        return E_CMX_SUCCESS;
    }
    else
    {
        LOG_ERROR_IF("unmap failed ptr=%p: %s", (void* )cmx_shm_ptr, strerror(errno));
        return E_CMX_OPERATION_FAILED;
    }
}
int cmx_shm_destroy(cmx_shm * const cmx_shm_ptr)
{
    char filename_buf[PATH_MAX];
    cmx_registry_format_filename(filename_buf, cmx_shm_ptr->process_id, cmx_shm_ptr->name);

    LOG_DEBUG_IF("Destroy CMX shared memory object ptr=%p component_name=%s", (void* )cmx_shm_ptr, cmx_shm_ptr->name);
    if (cmx_shm_unmap(cmx_shm_ptr) == E_CMX_SUCCESS)
    {
        if (shm_unlink(filename_buf) != 0)
        {
            LOG_ERROR_IF("unlink failed ptr=%p, filename=%s: %s", (void* )cmx_shm_ptr, filename_buf, strerror(errno));
            return E_CMX_OPERATION_FAILED;
        }
        return E_CMX_SUCCESS;
    }
    else
    {
        return E_CMX_OPERATION_FAILED;
    }
}

int cmx_shm_create(const char * const name, cmx_shm ** const cmx_shm_ptr)
{
    const int process_id = cmx_common_current_pid();
    int fd;
    char shm_filename[PATH_MAX];
    if (cmx_common_check_string_length(name, 1, CMX_NAME_SZ) == CMX_FALSE)
    {
        LOG_WARN_IF("Invalid length of argument 'name'");
        return E_CMX_INVALID_ARGUMENT;
    }
    if (cmx_shm_ptr == NULL)
    {
        LOG_WARN_IF("Argument 'cmx_shm_ptr' is NULL");
        return E_CMX_INVALID_ARGUMENT;
    }

    // format shm filename
    cmx_registry_format_filename(shm_filename, process_id, name);

    // try to create cmx object
    fd = shm_open(shm_filename, (O_CREAT | O_EXCL | O_RDWR),
            (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH /*666*/));
    if (fd >= 0)
    {
        // this block is now synchronized in-kernel because a second call to shm_open with O_EXCL fail
        LOG_DEBUG_IF("Create new shm object: '%s'", shm_filename);

        // set the initial size to sizeof(header), this is consistent with no_metrics=0
        if (ftruncate(fd, (off_t) sizeof(cmx_shm)) != 0)
        {
            LOG_ERROR_IF("Error ftruncate(): %s", strerror(errno));
            close(fd);
            return E_CMX_CREATE_FAILED;
        }

        cmx_shm * __cmx_shm_ptr = (cmx_shm *) mmap(0, sizeof(cmx_shm), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (__cmx_shm_ptr == MAP_FAILED)
        {
            LOG_ERROR_IF("Error mmap() shm: %s", strerror(errno));
            close(fd);
            shm_unlink(shm_filename);
            return E_CMX_CREATE_FAILED;
        }
        LOG_TRACE_IF("New shm object mmapped at %p", (void* )__cmx_shm_ptr);

        strncpy(__cmx_shm_ptr->name, name, CMX_NAME_SZ - 1);
        __cmx_shm_ptr->name[CMX_NAME_SZ - 1] = '\0';
        __cmx_shm_ptr->process_id = process_id;

        cmx_atomic_smp_wmb();
        // A valid CMX_TAG marks memory as initialized
        memcpy(__cmx_shm_ptr->CMX_TAG, CMX_SHM_TAG, CMX_SHM_TAG_LEN);

        // close file descriptor. It is no longer needed after the memory mmapped()
        // Thes might change if we support resizing.
        if (close(fd) != 0)
        {
            LOG_WARN_IF("close on fd=%d failed: %s", fd, strerror(errno));
        }

        *cmx_shm_ptr = __cmx_shm_ptr;
        return E_CMX_SUCCESS;
    }
    else
    {
        if (errno == EEXIST)
        {
            LOG_ERROR_IF("Error shm_open(%s): %s", shm_filename, strerror(errno));
            return E_CMX_NAME_EXISTS;
        }
        else
        {
            LOG_ERROR_IF("Error shm_open(%s): %s", shm_filename, strerror(errno));
            return E_CMX_CREATE_FAILED;
        }
    }
}

static int _cmx_shm_open(const int process_id, const char * const name, cmx_shm ** const cmx_shm_ptr, int oflag)
{
    int fd;
    char shm_filename[PATH_MAX];
    if (cmx_common_check_string_length(name, 1, CMX_NAME_SZ) == CMX_FALSE)
    {
        LOG_WARN_IF("Invalid length of string parameter name");
        return E_CMX_INVALID_ARGUMENT;
    }
    if (cmx_shm_ptr == NULL)
    {
        LOG_WARN_IF("Argument 'cmx_shm_ptr' is NULL");
        return E_CMX_INVALID_ARGUMENT;
    }

    // format shm filename
    cmx_registry_format_filename(shm_filename, process_id, name);

    // open existing object
    fd = shm_open(shm_filename, oflag, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH /*666*/));
    if (fd >= 0)
    {
        LOG_TRACE_IF("Open existing CMX memory object fd=%d: '%s'", fd, shm_filename);

        //check size >= header
        struct stat stat_buf;
        if (fstat(fd, &stat_buf) == -1)
        {
            LOG_ERROR_IF("fstat(fd=%d) failed: %s", fd, strerror(errno));
            close(fd);
            return E_CMX_OPERATION_FAILED;
        }
        if (stat_buf.st_size != (int) sizeof(cmx_shm))
        {
            LOG_ERROR_IF("Invalid shm object size fd=%d size=%lu expected size=%zu", fd, stat_buf.st_size,
                    sizeof(cmx_shm));
            close(fd);
            return E_CMX_OPERATION_FAILED;
        }

        int mmap_prot = PROT_READ;
        if (oflag & O_RDWR) mmap_prot |= PROT_WRITE;

        cmx_shm * __cmx_shm_ptr = mmap(0, sizeof(cmx_shm), mmap_prot, MAP_SHARED, fd, 0);
        if (__cmx_shm_ptr == MAP_FAILED)
        {
            LOG_ERROR_IF("Error mmap_prot=%d: %s\n", mmap_prot, strerror(errno));
            close(fd);
            return E_CMX_OPERATION_FAILED;
        }
        LOG_DEBUG_IF("Open existing CMX: mmapped at %p", (void* )__cmx_shm_ptr);

        if (memcmp(__cmx_shm_ptr->CMX_TAG, CMX_SHM_TAG, CMX_SHM_TAG_LEN) != 0)
        {
            LOG_ERROR_IF("Invalid header cmx reader, read TODO but expected TODO. "
                    "Maybe it is currently in initialization by another process/thread.");
            munmap(__cmx_shm_ptr, sizeof(cmx_shm));
            close(fd);
            return E_CMX_CORRUPT_SEGMENT;
        }

        if (close(fd) != 0)
        {
            LOG_WARN_IF("close on fd=%d failed: %s", fd, strerror(errno));
        }
        *cmx_shm_ptr = __cmx_shm_ptr;
        return E_CMX_SUCCESS;
    }
    else
    {
        LOG_ERROR_IF("Error shm_open(%s, oflag=%d): %s", shm_filename, oflag, strerror(errno));
        return E_CMX_NOT_FOUND;
    }
}

int cmx_shm_open(const int process_id, const char * const name, cmx_shm ** const cmx_shm_ptr)
{
    return _cmx_shm_open(process_id, name, cmx_shm_ptr, O_RDWR);
}

int cmx_shm_open_ro(const int process_id, const char * const name, cmx_shm ** const cmx_shm_ptr)
{
    return _cmx_shm_open(process_id, name, cmx_shm_ptr, O_RDONLY);
}

int cmx_shm_gc(cmx_shm * const cmx_shm_ptr)
{
    /*
     * go through all values
     * try to find blocks of values of > PAGESIZE.
     * Then mark them as gc-ed, call madvise MADV_REMOVE
     */
    return E_CMX_SUCCESS;
}

static int cmx_shm_alloc_slot(cmx_shm * const cmx_shm_ptr)
{
    for (int i = 0; i < CMX_NUMBER_OF_VALUES; i++)
    {
        if (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[i]) == CMX_SLOT_STATE_FREE)
        {
            if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[i], CMX_SLOT_STATE_FREE,
                    CMX_SLOT_STATE_OCCUPIED) == CMX_SLOT_STATE_FREE)
            {
                LOG_TRACE_IF("Allocate slot index=%d", i);
                return i;
            }
        }
    }
    return E_CMX_COMPONENT_FULL;
}

int cmx_shm_add_value_single(cmx_shm * const cmx_shm_ptr, const int type, const char * const name)
{
    if (cmx_shm_ptr == NULL)
    {
        LOG_WARN_IF("Argument 'cmx_shm_ptr' is NULL");
        return E_CMX_INVALID_ARGUMENT;
    }
    switch (type)
    {
        case CMX_TYPE_INT64:
        case CMX_TYPE_FLOAT64:
        case CMX_TYPE_BOOL:
            break;
        default:
            LOG_WARN_IF("Invalid type %d", type);
            return E_CMX_INVALID_ARGUMENT;
    }

    if (cmx_common_check_string_length(name, 1, CMX_NAME_SZ) == CMX_FALSE)
    {
        LOG_WARN_IF("Invalid length of string parameter name");
        return E_CMX_INVALID_ARGUMENT;
    }

    const int process_id = cmx_common_current_pid();
    if (cmx_atomic_read_int32(&cmx_shm_ptr->process_id) != process_id)
    {
        LOG_WARN_IF("Invalid pid");
        return E_CMX_INVALID_PID;
    }

    int value_index = cmx_shm_alloc_slot(cmx_shm_ptr);
    if (value_index == E_CMX_COMPONENT_FULL)
    {
        LOG_WARN_IF("Component full");
        return E_CMX_COMPONENT_FULL;
    }
    cmx_shm_slot_value * value = &cmx_shm_ptr->value[value_index].value;
    _next_id(&value->id);
    strncpy(value->name, name, CMX_NAME_SZ - 1);
    value->name[CMX_NAME_SZ - 1] = '\0';
    value->mtime = cmx_common_current_time_usec();
    value->type = type;
    memset(&value->value, '\0', sizeof(cmx_shm_value));

    cmx_atomic_smp_wmb();
    cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);

    return cmx_shm_pack_value_handle(value->id, value_index);
}

int cmx_shm_add_value_string(cmx_shm * const cmx_shm_ptr, const char * const name, const int size)
{
    if (cmx_shm_ptr == NULL)
    {
        LOG_WARN_IF("Argument 'cmx_shm_ptr' is NULL");
        return E_CMX_INVALID_ARGUMENT;
    }
    if (cmx_common_check_string_length(name, 1, CMX_NAME_SZ) == CMX_FALSE)
    {
        LOG_WARN_IF("Invalid length of string argument 'name'");
        return E_CMX_INVALID_ARGUMENT;
    }

    if (size > CMX_SHM_MAX_STRING_SZ)
    {
        LOG_WARN_IF("Invalid argument 'size'=%d", size);
        return E_CMX_INVALID_ARGUMENT;
    }
    if (size < 0)
    {
        LOG_WARN_IF("Invalid argument 'size'=%d", size);
        return E_CMX_INVALID_ARGUMENT;
    }

    const int process_id = cmx_common_current_pid();
    if (cmx_atomic_read_int32(&cmx_shm_ptr->process_id) != process_id)
    {
        LOG_WARN_IF("Invalid pid");
        return E_CMX_INVALID_PID;
    }

    int slot_index_head = cmx_shm_alloc_slot(cmx_shm_ptr);
    if (slot_index_head == E_CMX_COMPONENT_FULL)
    {
        LOG_WARN_IF("Component full: cannot allocate slot_index_head");
        return E_CMX_COMPONENT_FULL;
    }

    int no_payload_slots = (size / CMX_SHM_PAYLOAD_SZ) + 1;
    uint16_t * slot_indexes = (uint16_t*) malloc(sizeof(uint16_t) * (unsigned int) no_payload_slots);
    if (slot_indexes == NULL)
    {
        LOG_ERROR_IF("Out of memory while allocating space for slot_indexes");
        return E_CMX_OUT_OF_MEMORY;
    }

    // allocate slots
    for (int i = 0; i < no_payload_slots; i++)
    {
        int slot_index = cmx_shm_alloc_slot(cmx_shm_ptr);
        if (slot_index < 0)
        {
            LOG_WARN_IF("Component full, undo all slot allocations i=%d: %s", i, cmx_common_strerror(slot_index));
            // undo all previous slot allocations
            for (int j = i; j >= 0; j--)
            {
                int free_slot_index = slot_indexes[j];
                if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[free_slot_index],
                        CMX_SLOT_STATE_OCCUPIED, CMX_SLOT_STATE_FREE) == CMX_SLOT_STATE_OCCUPIED)
                {
                    LOG_DEBUG_IF("set slot index=%d CMX_SLOT_STATE_FREE", free_slot_index);
                }
                else
                {
                    LOG_AUDIT_IF("Logic error, this state is unexpected. value_state[%d]=%d", free_slot_index,
                            cmx_atomic_read_int32(&cmx_shm_ptr->value_state[free_slot_index]));
                }
            }
            free(slot_indexes);
            return E_CMX_COMPONENT_FULL;
        }
        else
        {
            slot_indexes[i] = (uint16_t) slot_index;
        }
    }
    LOG_TRACE_IF("Allocated %d payload slots for new value named %s of size=%d", no_payload_slots, name, size);

    // setup head
    cmx_shm_slot_value * value_slot = &cmx_shm_ptr->value[slot_index_head].value;
    _next_id(&value_slot->id);
    strncpy(value_slot->name, name, CMX_NAME_SZ - 1);
    value_slot->name[CMX_NAME_SZ - 1] = '\0';
    value_slot->mtime = cmx_common_current_time_usec();
    value_slot->type = CMX_TYPE_STRING;

    for (int i = 0; i < no_payload_slots; i++)
    {
        int slot_index_payload = slot_indexes[i];
        cmx_shm_slot_payload * payload_slot = &cmx_shm_ptr->value[slot_index_payload].value_payload;
        _next_id(&payload_slot->id);
        memset(payload_slot->data, '\0', CMX_SHM_PAYLOAD_SZ);
    }

    // wire next_id's
    value_slot->value._string.next_index = slot_indexes[0];
    value_slot->value._string.next_id = cmx_shm_ptr->value[value_slot->value._string.next_index].value_payload.id;
    value_slot->value._string.size = (uint16_t) size; //cast protected by size check agains CMX_MAX_STRING_SZ
    value_slot->value._string.current_size = 0;

    for (int i = 0; i < no_payload_slots; i++)
    {
        int slot_index_payload = slot_indexes[i];
        cmx_shm_slot_payload * payload_slot = &cmx_shm_ptr->value[slot_index_payload].value_payload;
        if (i < no_payload_slots - 1)
        {
            payload_slot->next_index = slot_indexes[i + 1];
            payload_slot->next_id = cmx_shm_ptr->value[payload_slot->next_index].value_payload.id;
        }
        else
        {
            // last slot
            payload_slot->next_index = CMX_STRING_END_INDEX;
            payload_slot->next_id = 0;
        }
    }

    // set payload states to CMX_SLOT_STATE_PAYLOAD
    for (int i = 0; i < no_payload_slots; i++)
    {
        int slot_index_payload = slot_indexes[i];

        if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[slot_index_payload],
                CMX_SLOT_STATE_OCCUPIED, CMX_SLOT_STATE_PAYLOAD) == CMX_SLOT_STATE_OCCUPIED)
        {
            // OK
        }
        else
        {
            LOG_AUDIT_IF("Logic error, this state is unexpected. value_state[%d]=%d", slot_index_payload,
                    cmx_atomic_read_int32(&cmx_shm_ptr->value_state[slot_index_payload]));
        }
    }

    // set head slot/value_slot to CMX_SLOT_STATE_SET
    cmx_atomic_smp_wmb();
    cmx_atomic_set_int32(&cmx_shm_ptr->value_state[slot_index_head], CMX_SLOT_STATE_SET);

    free(slot_indexes);

    return cmx_shm_pack_value_handle(value_slot->id, slot_index_head);
}

int cmx_shm_set_value_single(cmx_shm * const cmx_shm_ptr, const int value_handle, const int type,
        const cmx_shm_value * const value)
{
    const int process_id = cmx_common_current_pid();
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);

    // check arguments
    _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, value_index);

    if (value == NULL)
    {
        LOG_WARN_IF("argument 'value' is NULL. value_handle=%d", value_handle);
        return E_CMX_INVALID_ARGUMENT;
    }

    // check that the process is 'owner' of this component
    if (cmx_atomic_read_int32(&cmx_shm_ptr->process_id) != process_id)
    {
        LOG_WARN_IF("Invalid pid");
        return E_CMX_INVALID_PID;
    }

    // State = T_W_1
    // Enter state UPDATE
    switch (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET,
            CMX_SLOT_STATE_UPDATE))
    {
        case CMX_SLOT_STATE_SET:
            // thats OK, the XCHG succeeded
            break;
        case CMX_SLOT_STATE_UPDATE:
            // another update is in progress
            return E_CMX_CONCURRENT_MODIFICATION;
        case CMX_SLOT_STATE_FREE:
            // attempted to set() on empty slot
            return E_CMX_INVALID_HANDLE;
        default:
            // invalid state
            return E_CMX_OPERATION_FAILED;
    }

    // check the handle id is valid
    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        LOG_WARN_IF("Invalid handle");
        // return to "SET" state
        cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);
        return E_CMX_INVALID_HANDLE;
    }

    // check the type
    if (cmx_atomic_read_int32(&cmx_shm_ptr->value[value_index].value.type) != type)
    {
        // the slot has a different type than the one requested by argument 'type'
        // return to "SET" state
        cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);
        return E_CMX_TYPE_MISMATCH;
    }

    // State = T_W_2
    // increase txn counter
    cmx_atomic_set_uint64(&cmx_shm_ptr->value[value_index].value.txn,
            cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn) + 1);
    cmx_atomic_smp_wmb();

    // State = T_W_3
    // update value
    memcpy(&cmx_shm_ptr->value[value_index].value.value, value, sizeof(cmx_shm_value));

    // update mtime
    cmx_atomic_set_uint64(&cmx_shm_ptr->value[value_index].value.mtime, cmx_common_current_time_usec());

    // State = T_W_4
    // Leave "UPDATE" / enter "SET" state
    if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_UPDATE,
            CMX_SLOT_STATE_SET) != CMX_SLOT_STATE_UPDATE)
    {
        // some-kind of fatal concurrent modification
        LOG_AUDIT_IF("Program flow error/memory corruption");
        return E_CMX_OPERATION_FAILED;
    }
    return E_CMX_SUCCESS;
}

int cmx_shm_set_value_string(cmx_shm * const cmx_shm_ptr, const int value_handle, const char * const value,
        const int len)
{
    const int process_id = cmx_common_current_pid();
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);

    // check arguments
    _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, value_index);

    if (value == NULL)
    {
        LOG_WARN_IF("argument 'value' is NULL. value_handle=%d", value_handle);
        return E_CMX_INVALID_ARGUMENT;
    }

    // check that the process is 'owner' of this component
    if (cmx_atomic_read_int32(&cmx_shm_ptr->process_id) != process_id)
    {
        LOG_WARN_IF("Invalid pid");
        return E_CMX_INVALID_PID;
    }

    // State = T_W_1
    // enter state UPDATE
    switch (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET,
            CMX_SLOT_STATE_UPDATE))
    {
        case CMX_SLOT_STATE_SET:
            // OK, XCHG succeeded
            break;
        case CMX_SLOT_STATE_UPDATE:
            // another update in progress
            return E_CMX_CONCURRENT_MODIFICATION;
        case CMX_SLOT_STATE_FREE:
            // attempted to set() on empty slot
            return E_CMX_INVALID_HANDLE;
        default:
            // invalid state
            return E_CMX_OPERATION_FAILED;
    }

    cmx_atomic_smp_rmb();

    // check the handle id is valid
    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        LOG_WARN_IF("Invalid handle");
        // return to "SET" state
        cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);
        return E_CMX_INVALID_HANDLE;
    }

    // check the type
    if (cmx_atomic_read_int32(&cmx_shm_ptr->value[value_index].value.type) != CMX_TYPE_STRING)
    {
        // the slot has a different type than the one requested by argument 'type'
        // return to "SET" state
        cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);
        return E_CMX_TYPE_MISMATCH;
    }

    // State = T_W_2
    // increase txn counter
    cmx_atomic_set_uint64(&cmx_shm_ptr->value[value_index].value.txn,
            cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn) + 1);
    cmx_atomic_smp_wmb();

    cmx_shm_slot_value * const value_slot = &cmx_shm_ptr->value[value_index].value;

    // check requested string len with maximal size of this slot
    if (len > value_slot->value._string.size)
    {
        LOG_WARN_IF("Invalid String length. Maximum: %hu bytes, requested: %d. value_handle=%d",
                value_slot->value._string.size, len, value_handle);
        // return to "SET" state
        cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET);
        return E_CMX_INVALID_ARGUMENT;
    }

    // State = T_W_3
    int payload_index_next = value_slot->value._string.next_index;
    int payload_id_next = value_slot->value._string.next_id;
    cmx_shm_slot_payload * payload_slot;

    const char * value_pos = value;
    uint16_t bytes_to_copy = (uint16_t) len; //cast is protected by previous size-check
    value_slot->value._string.current_size = bytes_to_copy;

    while (bytes_to_copy > 0)
    {
        payload_slot = &cmx_shm_ptr->value[payload_index_next].value_payload;

        if (payload_slot->id != payload_id_next)
        {
            LOG_WARN_IF("'id' corruption at index_next=%d id_next=%d", payload_index_next, payload_id_next);
            // enter state UPDATE
            if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_UPDATE,
                    CMX_SLOT_STATE_SET) != CMX_SLOT_STATE_UPDATE)
            {
                LOG_AUDIT_IF("Program flow error/memory corruption");
            }
            return E_CMX_INVALID_HANDLE;
        }

        if (bytes_to_copy < CMX_SHM_PAYLOAD_SZ)
        {
            memcpy(payload_slot->data, value_pos, bytes_to_copy);
            // put a \0 after the data to make every chunk a valid c-string.
            payload_slot->data[bytes_to_copy] = '\0';
            bytes_to_copy = 0;
        }
        else
        {
            memcpy(payload_slot->data, value_pos, CMX_SHM_PAYLOAD_SZ);
            // put a \0 after the data to stop c from accidental reading to much
            payload_slot->data[CMX_SHM_PAYLOAD_SZ] = '\0';
            value_pos += CMX_SHM_PAYLOAD_SZ;
            bytes_to_copy = (uint16_t) (bytes_to_copy - CMX_SHM_PAYLOAD_SZ);
        }

        payload_id_next = payload_slot->next_id;
        payload_index_next = payload_slot->next_index;
    }

    // update mtime
    cmx_atomic_set_uint64(&cmx_shm_ptr->value[value_index].value.mtime, cmx_common_current_time_usec());

    // State = T_W_4
    // Leave "UPDATE" / enter "SET" state
    if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_UPDATE,
            CMX_SLOT_STATE_SET) != CMX_SLOT_STATE_UPDATE)
    {
        // some-kind of fatal concurrent modification
        LOG_AUDIT_IF("Program flow error/memory corruption");
        return E_CMX_OPERATION_FAILED;
    }
    return E_CMX_SUCCESS;
}

int cmx_shm_find_value(cmx_shm * const cmx_shm_ptr, int start, int * value_handle, int * value_type)
{
    uint16_t i_value_id;

    _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, start);

    if (value_handle == NULL || value_type == NULL)
    {
        LOG_WARN_IF("Either argument value_handle=%p or value_type=%p is NULL", value_handle, value_type);
        return E_CMX_INVALID_ARGUMENT;
    }

    for (int i = start; i < CMX_NUMBER_OF_VALUES; i++)
    {
        cmx_atomic_smp_rmb();
        switch (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[i]))
        {
            case CMX_SLOT_STATE_SET:
            case CMX_SLOT_STATE_UPDATE:
                // Slot contains a value
                // State UPDATE is also accepted because we
                // assume that at the time the caller is really going to
                // read the value, the state will be back to SET
                i_value_id = cmx_shm_ptr->value[i].value.id;
                *value_handle = cmx_shm_pack_value_handle(i_value_id, i);
                *value_type = cmx_shm_ptr->value[i].value.type;
                LOG_TRACE_IF("start=%d stop=%d", start, i);
                return E_CMX_SUCCESS;
            default:
                continue;
        }
    }

    LOG_TRACE_IF("start=%d E_CMX_NOT_FOUND", start);
    return E_CMX_NOT_FOUND;
}

int cmx_shm_get_value_single(const cmx_shm * const cmx_shm_ptr, const int value_handle, int * const type,
        cmx_shm_value * const value, uint64_t * const mtime)
{
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);
    uint64_t txn_start;

    _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, value_index);

    // State = T_R_1
    // Save txn counter on entry
    txn_start = cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn);

    // force all reads to execute before reading the 'value_state' and 'value_id'
    // to enforce the ordering between T_R_1 and T_R_2
    cmx_atomic_smp_rmb();

    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        return E_CMX_INVALID_HANDLE;
    }

    // State = T_R_2
    switch (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index]))
    {
        case CMX_SLOT_STATE_SET:
            // Slot is marked 'set'
            break;
        case CMX_SLOT_STATE_UPDATE:
            return E_CMX_CONCURRENT_MODIFICATION;
        default:
            return E_CMX_INVALID_HANDLE;
    }

    // State = T_R_3
    // copy the value
    cmx_atomic_smp_rmb();
    if (value) memcpy(value, &cmx_shm_ptr->value[value_index].value.value, sizeof(cmx_shm_value));
    if (type) *type = cmx_shm_ptr->value[value_index].value.type;
    if (mtime) *mtime = cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.mtime);

    // Force all reads to be executed before reading txn^k
    // This prevents that we read a 'new' value with an 'old' txn.
    cmx_atomic_smp_rmb();

    // State = T_R_4
    // State = T_R_5
    // Check that no updates happened in the meantime
    if (cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn) != txn_start)
    {
        return E_CMX_CONCURRENT_MODIFICATION;
    }

    return E_CMX_SUCCESS;
}

int cmx_shm_get_value_string(const cmx_shm * const cmx_shm_ptr, const int value_handle, cmx_string_callback cb,
        void * const arg, uint64_t * const mtime)
{
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);
    uint64_t txn_start;
    int value_index_next;
    int value_id_next;

    _CHECK_VALUE_INDEX_RETURN_IF_INVALID(cmx_shm_ptr, value_index);

    // State = T_R_1
    // Save txn counter on entry
    txn_start = cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn);

    // force all reads to execute before reading the 'value_state' and 'value_id'
    // to enforce the ordering between T_R_1 and T_R_2
    cmx_atomic_smp_rmb();

    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        return E_CMX_INVALID_HANDLE;
    }

    // State = T_R_2
    switch (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index]))
    {
        case CMX_SLOT_STATE_SET:
            // ok, probably good. will be checked again at the end
            break;
        case CMX_SLOT_STATE_UPDATE:
            return E_CMX_CONCURRENT_MODIFICATION;
        default:
            return E_CMX_INVALID_HANDLE;
    }

    // State = T_R_3
    const cmx_shm_slot_value * const value_slot = &cmx_shm_ptr->value[value_index].value;
    if (value_slot->type != CMX_TYPE_STRING)
    {
        LOG_WARN_IF("Invalid type, expected %d but slot is of type %d", CMX_TYPE_STRING, value_slot->type);
        return E_CMX_TYPE_MISMATCH;
    }

    value_index_next = value_slot->value._string.next_index;
    value_id_next = value_slot->value._string.next_id;

    uint16_t to_copy = value_slot->value._string.current_size;

    while (value_index_next != CMX_STRING_END_INDEX)
    {
        if (value_index_next < 0 || value_index_next >= CMX_NUMBER_OF_VALUES)
        {
            LOG_ERROR_IF("Corrupt segment, process_id=%d, value_handle=%d\n", cmx_shm_ptr->process_id, value_handle);
            return E_CMX_CORRUPT_SEGMENT;
        }
        const cmx_shm_slot_payload * payload_slot = &cmx_shm_ptr->value[value_index_next].value_payload;
        if (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index_next]) != CMX_SLOT_STATE_PAYLOAD)
        {
            // This is either a data corruption or the slot is being removed right now.
            return E_CMX_CONCURRENT_MODIFICATION;
        }
        if (cmx_atomic_read_uint16(&payload_slot->id) != value_id_next)
        {
            LOG_WARN_IF("'id' corruption at index %d", value_index_next);
            return E_CMX_INVALID_HANDLE;
        }

        const uint16_t chunksize = (to_copy < CMX_SHM_PAYLOAD_SZ) ? to_copy : CMX_SHM_PAYLOAD_SZ;
        // call the provided callback function
        if (chunksize > 0) cb(payload_slot->data, chunksize, arg);

        to_copy = (uint16_t) (to_copy - chunksize);
        value_index_next = payload_slot->next_index;
        value_id_next = payload_slot->next_id;
    }

    if (mtime) *mtime = cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.mtime);

    // Force all reads to be executed before reading txn^k
    // This prevents that we read a 'new' value with an 'old' txn.
    cmx_atomic_smp_rmb();

    // State = T_R_4
    // State = T_R_5
    // Check that no updates happened in the meantime
    if (cmx_atomic_read_uint64(&cmx_shm_ptr->value[value_index].value.txn) != txn_start)
    {
        return E_CMX_CONCURRENT_MODIFICATION;
    }

    return E_CMX_SUCCESS;
}

int cmx_shm_get_value_name(const cmx_shm * const cmx_shm_ptr, const int value_handle, char * const buf)
{
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);

    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        LOG_WARN_IF("Invalid handle. value_id expected: %d != %d (actual)", value_id,
                cmx_shm_ptr->value[value_index].value.id);
        return E_CMX_INVALID_HANDLE;
    }

    switch (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index]))
    {
        case CMX_SLOT_STATE_SET:
        case CMX_SLOT_STATE_UPDATE:
            // continue
            break;
        default:
            return E_CMX_INVALID_HANDLE;
    }

    memcpy(buf, &cmx_shm_ptr->value[value_index].value.name, CMX_NAME_SZ);

    cmx_atomic_smp_rmb();
    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        LOG_WARN_IF("Invalid handle. value_id expected: %d != %d (actual)", value_id,
                cmx_shm_ptr->value[value_index].value.id);
        return E_CMX_INVALID_HANDLE;
    }
    switch (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index]))
    {
        case CMX_SLOT_STATE_SET:
        case CMX_SLOT_STATE_UPDATE:
            // continue
            break;
        default:
            return E_CMX_INVALID_HANDLE;
    }

    return E_CMX_SUCCESS;
}

int cmx_shm_remove_value(cmx_shm * const cmx_shm_ptr, const int value_handle)
{
    const int process_id = cmx_common_current_pid();
    const int value_index = cmx_shm_unpack_index(value_handle);
    const uint16_t value_id = cmx_shm_unpack_id(value_handle);

    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        LOG_WARN_IF("Invalid handle. value_id expected: %d != %d (actual)", value_id,
                cmx_shm_ptr->value[value_index].value.id);
        return E_CMX_INVALID_HANDLE;
    }

    if (cmx_atomic_read_int32(&cmx_shm_ptr->process_id) != process_id)
    {
        LOG_WARN_IF("Invalid pid");
        return E_CMX_INVALID_PID;
    }

    // enter state OCCUPIED
    if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_SET,
            CMX_SLOT_STATE_OCCUPIED) != CMX_SLOT_STATE_SET)
    {
        return E_CMX_CONCURRENT_MODIFICATION;
    }

    // recheck ID
    cmx_atomic_smp_rmb();
    if (cmx_atomic_read_uint16(&cmx_shm_ptr->value[value_index].value.id) != value_id)
    {
        // Other thread removed and readded in the very small timeframe between first id
        // check and switching state.
        // re-enter state SET
        if (cmx_atomic_val_compare_and_swap_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_OCCUPIED,
                CMX_SLOT_STATE_SET) != CMX_SLOT_STATE_OCCUPIED)
        {
            LOG_AUDIT_IF("Logic error");
        }
        return E_CMX_INVALID_HANDLE;
    }

    cmx_shm_slot_value * value_slot = &cmx_shm_ptr->value[value_index].value;

    // change txn
    cmx_atomic_set_uint64(&value_slot->txn, cmx_atomic_read_uint64(&value_slot->txn) + 1);

    // change id
    _next_id(&value_slot->id);

    // separate change of txn/id from resetting data
    cmx_atomic_smp_wmb();

    // cleanup and reset all data
    LOG_TRACE_IF("Free value_slot value_index=%d", value_index);

    if (value_slot->type == CMX_TYPE_INT64 || value_slot->type == CMX_TYPE_FLOAT64 || value_slot->type == CMX_TYPE_BOOL)
    {
        value_slot->name[CMX_NAME_SZ - 1] = '\0';
        value_slot->mtime = 0ULL;
    }
    else if (value_slot->type == CMX_TYPE_STRING)
    {
        value_slot->name[CMX_NAME_SZ - 1] = '\0';
        value_slot->mtime = 0ULL;

        // cleanup the associated payload value_slot
        // since the 'head' of the value is already marked 'OCCUPIED'
        // we are safe the switch PAYLOAD slots imediately to 'FREE'
        int value_index_next = value_slot->value._string.next_index;
        int value_id_next = value_slot->value._string.next_id;
        int no_freed_payload_slots = 0;

        while (value_index_next != CMX_STRING_END_INDEX)
        {
            if (value_index_next >= CMX_NUMBER_OF_VALUES)
            {
                LOG_ERROR_IF("Corrupt value/payload-slot: value_index_next=%d value_id_next=%d\n", value_index_next,
                        value_id_next);
                return E_CMX_CORRUPT_SEGMENT;
            }
            cmx_shm_slot_payload * payload_slot = &cmx_shm_ptr->value[value_index_next].value_payload;
            if (cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index_next]) != CMX_SLOT_STATE_PAYLOAD)
            {
                LOG_ERROR_IF("Corrupt value/payload-slot, state %d != PAYLOAD: value_index_next=%d value_id_next=%d\n",
                        cmx_atomic_read_int32(&cmx_shm_ptr->value_state[value_index_next]), value_index_next,
                        value_id_next);
                return E_CMX_CORRUPT_SEGMENT;
            }
            if (cmx_atomic_read_uint16(&payload_slot->id) != value_id_next)
            {
                LOG_WARN_IF("Corrupt value/payload-slot, 'id' corruption:  value_index_next=%d value_id_next=%d\n",
                        value_index_next, value_id_next);
                return E_CMX_INVALID_HANDLE;
            }

            memset(&payload_slot->data, 0, CMX_SHM_PAYLOAD_SZ + 1);

            int value_index_current = value_index_next;
            value_index_next = payload_slot->next_index;
            value_id_next = payload_slot->next_id;
            payload_slot->next_index = CMX_STRING_END_INDEX;
            payload_slot->next_id = 0;

            cmx_atomic_smp_wmb();
            cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index_current], CMX_SLOT_STATE_FREE);
            no_freed_payload_slots++;
            LOG_TRACE_IF("Freed payload_slot value_index=%d set CMX_SLOT_STATE_FREE", value_index_current);

        }
        if ((value_slot->value._string.size / CMX_SHM_PAYLOAD_SZ) + 1 > no_freed_payload_slots)
        {
            LOG_ERROR_IF(
                    "Freed less payload slots than expected, (value_slot->value._string.size / CMX_SHM_PAYLOAD_SZ) + 1=%d no_freed_payload_slots=%d",
                    (value_slot->value._string.size / CMX_SHM_PAYLOAD_SZ) + 1, no_freed_payload_slots);
            return E_CMX_CORRUPT_SEGMENT;
        }
    }
    else
    {
        LOG_ERROR_IF("Called remove on slot index=%d with invalid type=%d", value_index, value_slot->type);
        return E_CMX_OPERATION_FAILED;
    }
    // enter state FREE
    cmx_atomic_smp_wmb();
    cmx_atomic_set_int32(&cmx_shm_ptr->value_state[value_index], CMX_SLOT_STATE_FREE);
    LOG_TRACE_IF("Freed value_slot value_index=%d set CMX_SLOT_STATE_FREE", value_index);

    return E_CMX_SUCCESS;
}
