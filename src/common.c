/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#include "cmw-cmx/common.h"
#include "cmw-cmx/log.h"

// -------------------------------------------------
//          PRIVATE FUNCTIONS START HERE
// -------------------------------------------------

// -------------------------------------------------
//       REGULAR FUNCTIONS START HERE
// -------------------------------------------------

long int cmx_cmx_lock_timeout_ns = 100000000L;

struct _errorcode_message_t
{
    int code;
    const char * message;
};

static struct _errorcode_message_t _errorcode_message[] =
{ //
        { E_CMX_SUCCESS, "Success, no error." },
        { E_CMX_OPERATION_FAILED, "Operation failed" },
        { E_CMX_INVALID_HANDLE, "The component reference is invalid (invalid counter value)"},
        { E_CMX_TYPE_MISMATCH, "Type mismatch, accessed element has different cmx datatype" },
        { E_CMX_CORRUPT_SEGMENT, "component is corrupted" },
        { E_CMX_INVALID_ARGUMENT, "A function argument is invalid (e.g. invalid string size or value)" },
        { E_CMX_OUT_OF_MEMORY, "out of memory. malloc() failed" },
        { E_CMX_CREATE_FAILED, "Creation of component failed" },
        { E_CMX_INVALID_PID, "Given segment is invalid (belongs to other process)" },
        { E_CMX_CONCURRENT_MODIFICATION, "The request failed because of concurrent access" },
        { E_CMX_NAME_EXISTS, "Metric or property with same name exists" },
        { E_CMX_NOT_FOUND, "The metric or component with the given handle/name cannot be found" },
        { E_CMX_COMPONENT_FULL, "Failed to add more metrics, the component is full" },
        { E_CMX_PROTOCOL_VERSION, "Unknown component protocol version" },
        { E_CMX_OUT_OF_RANGE, "element index out of range" },
        { E_CMX_UNUSED_3, "Unused/deprecated errorcode E_CMX_UNUSED_3" },
        { E_CMX_UNUSED_1, "Unused/deprecated errorcode E_CMX_UNUSED_1" },
        { E_CMX_UNUSED_2, "Unused/deprecated errorcode E_CMX_UNUSED_2"},
        { 0, NULL } };

const char * cmx_common_strerror(const int code)
{
    for (int i = 0; _errorcode_message[i].message != NULL; i++)
    {
        if (_errorcode_message[i].code == code)
        {
            return _errorcode_message[i].message;
        }
    }
    return NULL;
}


int cmx_common_check_string_length(const char * string, unsigned int min_size, unsigned int max_size)
{
    if (string == NULL)
    {
        return CMX_FALSE;
    }
    size_t name_len = strnlen(string, max_size);
    if (name_len < min_size)
    {
        LOG_TRACE_IF("Name must be at least %d chars in size", min_size);
        return CMX_FALSE;
    }
    else if (name_len == max_size)
    {
        LOG_TRACE_IF("Name exceeds maximum size %d", max_size);
        return CMX_FALSE;
    }

    return CMX_TRUE;
}

uint64_t cmx_common_current_time_usec(void)
{
    struct timespec ts;
    // POSIX Documentation on clock_gettime(): "CLOCK_REALTIME is in <time.h>. The values returned by
    //      clock_gettime() represent the amount of time (in seconds and nanoseconds) since the Epoch"
    if (clock_gettime(CLOCK_REALTIME, &ts) != 0)
    {
        LOG_ERROR_IF("clock_gettime() failed: %s", strerror(errno));
        return 0;
    }
    unsigned long long seconds = 1000ULL * 1000ULL * (unsigned long long) ts.tv_sec;
    unsigned long microseconds = (unsigned long) (ts.tv_nsec / 1000L);
    return seconds + microseconds;
}


int cmx_common_current_pid(void)
{
    return (int) getpid();
}
