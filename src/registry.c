/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "cmw-cmx/registry.h"
#include "cmw-cmx/common.h"
#include "cmw-cmx/log.h"

/**
 * Set the Information in cmx_component_info by reading
 *  - the CMX_TAG in the shared memory
 *  - parse the filename (name, pid)
 */
static int _cmx_registry_set_component_info(cmx_component_info * const component, const struct dirent * entry)
{
    struct stat stat_buf;
    int fd;

    ////// CMX TAG
    // open shared memory object. get a filedescriptor
    fd = shm_open(entry->d_name, O_RDONLY, 0);
    if (fd < 0)
    {
        LOG_DEBUG_IF("Failed to open shm %s: %s", entry->d_name, strerror(errno));
        return E_CMX_OPERATION_FAILED;
    }
    // check filesize  not < 8
    if (fstat(fd, &stat_buf) == -1)
    {
        LOG_DEBUG_IF("Failed to fstat %s: %s", entry->d_name, strerror(errno));
        close(fd);
        return E_CMX_OPERATION_FAILED;
    }

    if (stat_buf.st_size < 8)
    {
        LOG_DEBUG_IF("CMX SHM Object is too small %s", entry->d_name);
        close(fd);
        return E_CMX_OPERATION_FAILED;
    }

    void * mem = mmap(0, 8, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mem == MAP_FAILED)
    {
        LOG_DEBUG_IF("Failed to mmap %s: %s", entry->d_name, strerror(errno));
        close(fd);
        return E_CMX_OPERATION_FAILED;
    }
    // can close fd after mmap
    close(fd);

    // copy cmx_tag
    memcpy(component->CMX_TAG, mem, CMX_SHM_TAG_LEN);

    // unmap
    munmap(mem, 8);

    //// Name and process id from filename
    if (cmx_registry_parse_filename(entry->d_name, &component->process_id, component->name) == CMX_TRUE)
    {
        return E_CMX_SUCCESS;
    }
    else
    {
        return E_CMX_OPERATION_FAILED;
    }
}

/**
 * Check if entry->d_name starts with "cmx."
 */
static int _cmx_registry_filter_func(const struct dirent * entry)
{
    int r = (strstr(entry->d_name, "cmx.") == entry->d_name);
    if (r) LOG_TRACE_IF("Found %s", entry->d_name);
    return r;
}

int cmx_registry_discover(cmx_component_info ** const p_components, int * const len)
{
    cmx_component_info * components;
    struct dirent ** dev_shm_entries = NULL;
    int len_entries;
    int index_components;

    // Check arguments
    if (len == NULL)
    {
        LOG_WARN_IF("Invalid argument len=NULL");
        return E_CMX_INVALID_ARGUMENT;
    }

    len_entries = scandir("/dev/shm", &dev_shm_entries, _cmx_registry_filter_func, alphasort);
    if (len_entries < 0)
    {
        if (p_components) *p_components = NULL;
        LOG_WARN_IF("CMX discovery failed: %s", strerror(errno));
        return E_CMX_OPERATION_FAILED;
    }

    // reserve no. of components + 1 (never allocate 0 bytes)
    components = (cmx_component_info*) malloc(sizeof(cmx_component_info) * (unsigned int) (len_entries + 1));
    if (components == NULL)
    {
        LOG_ERROR_IF("CMX out of memory: %s", strerror(errno));
        for (int i = 0; i < len_entries; i++)
            free(dev_shm_entries[i]);
        free(dev_shm_entries);
        return E_CMX_OUT_OF_MEMORY;
    }
    memset(components, '\0', sizeof(cmx_component_info) * (unsigned int) (len_entries + 1));

    index_components = 0;
    *len = 0;
    for (int index_entries = 0; index_entries < len_entries; index_entries++)
    {
        struct dirent * entry = dev_shm_entries[index_entries];
        if (_cmx_registry_set_component_info(&components[index_components], entry) == E_CMX_SUCCESS)
        {
            index_components++;
            (*len)++;
        }
        free(entry);
    }
    free(dev_shm_entries);

    if (p_components) *p_components = components;
    else free(components);

    return E_CMX_SUCCESS;
}

void cmx_registry_format_filename(char * filename_buf, const int process_id, const char * const component_name)
{
    sprintf(filename_buf, "cmx.%d.%.*s", process_id, 63, component_name);
}

int cmx_registry_parse_filename(const char * filename_buf, int * const process_id, char * const component_name)
{
    if (sscanf(filename_buf, "cmx.%d.%63s", process_id, component_name) == 2)
    {
        return CMX_TRUE;
    }
    else
    {
        return CMX_FALSE;
    }
}

int cmx_registry_cleanup(void)
{
    char path_buf_pid[PATH_MAX];
    char filename_buf_shm[PATH_MAX];
    struct stat stat_buf;
    cmx_component_info * components;
    int len;
    int r;

    r = cmx_registry_discover(&components, &len);
    if (r != E_CMX_SUCCESS)
    {
        return r;
    }

    for (int i = 0; i < len; i++)
    {
        cmx_component_info * component = &(components[i]);
        // check if process exists
        sprintf(path_buf_pid, "/proc/%d", component->process_id);
        if (stat(path_buf_pid, &stat_buf) == -1 && errno == ENOENT)
        {
            // process don't exist anymore
            LOG_DEBUG_IF("Cleanup component %s from dead process %d", component->name, component->process_id);
            cmx_registry_format_filename(filename_buf_shm, component->process_id, component->name);
            if (shm_unlink(filename_buf_shm) != 0)
            {
                LOG_INFO_IF("Cleanup failed: %s", strerror(errno));
            }
        }
    }
    free(components);

    return E_CMX_SUCCESS;
}
