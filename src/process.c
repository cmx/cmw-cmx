#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <sys/resource.h>

#include "cmw-cmx/registry.h"
#include "cmw-cmx/process.h"
#include "cmw-cmx/shm.h"
#include "cmw-cmx/log.h"

#define CMW_CMX_USE_PRIVATE
#include "cmw-cmx/atomic.h"

static cmx_shm * process_shm;
static int32_t process_shm_once_pid = 0;

#ifdef MANIFEST_INFO
extern char _manifest_start;
extern char _manifest_end;
#endif

struct cmx_process_metric_t
{
    const char * name;
    void (*function)(cmx_shm * const /*cmx_shm_ptr*/, struct cmx_process_metric_t * const /*process_metric*/);
    int value_handle;
};

#ifdef MANIFEST_INFO
/**
 * Read the manifest variable. Truncate if length > CMX_SHM_MAX_STRING_SZ.
 *
 * If the manifest is truncated then it ends with '\n$...$'.
 */
static void _metric_manifest(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    if (process_metric->value_handle == 0)
    {
        ptrdiff_t manifest_len = &_manifest_end - &_manifest_start;
        int is_truncated = 0;
        if (manifest_len > CMX_SHM_MAX_STRING_SZ)
        {
            LOG_INFO_IF("Manifest truncated, original length=%td", manifest_len);
            is_truncated = 1;
            manifest_len = CMX_SHM_MAX_STRING_SZ;
        }
        if (manifest_len > 10 && manifest_len <= CMX_SHM_MAX_STRING_SZ)
        {
            char * manifest_buf;
            manifest_buf = (char*) malloc(sizeof(char) * manifest_len + 1);
            if (manifest_buf == NULL)
            {
                process_metric->value_handle = E_CMX_OUT_OF_MEMORY;
                LOG_WARN_IF("Failed to add manifest: malloc() failed");
                return;
            }
            strncpy(manifest_buf, &_manifest_start, (size_t) manifest_len);
            if (is_truncated)
            {
                // end with '\n$...$' to indicate that this manifest is truncated
                manifest_buf[manifest_len - 6] = '\n';
                manifest_buf[manifest_len - 5] = '$';
                manifest_buf[manifest_len - 4] = '.';
                manifest_buf[manifest_len - 3] = '.';
                manifest_buf[manifest_len - 2] = '.';
                manifest_buf[manifest_len - 1] = '$';
            }

            process_metric->value_handle = cmx_shm_add_value_string(cmx_shm_ptr, process_metric->name, manifest_len);
            cmx_shm_set_value_string(cmx_shm_ptr, process_metric->value_handle, manifest_buf, manifest_len);
            free(manifest_buf);
        }
        else
        {
            // setting value_handle
            process_metric->value_handle = E_CMX_INVALID_ARGUMENT;
            LOG_WARN_IF("Failed to add manifest: Invalid manifest length=%td. Maximum length=%d", manifest_len,
                    CMX_SHM_MAX_STRING_SZ);
        }
    }
}
#endif

extern char * program_invocation_short_name;

static void _metric_process_name(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    if (process_metric->value_handle == 0)
    {
        process_metric->value_handle = cmx_shm_add_value_string(cmx_shm_ptr, process_metric->name, 256);
        cmx_shm_set_value_string(cmx_shm_ptr, process_metric->value_handle, program_invocation_short_name,
                (int) strnlen(program_invocation_short_name, 255) + 1);
    }
}

static void _metric_process_id(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    if (process_metric->value_handle == 0)
    {
        cmx_shm_value value = //
                { ._int64 = cmx_common_current_pid() };
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static void _metric_hostname(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    char hostname_buf[512];
    if (process_metric->value_handle == 0)
    {
        memset(hostname_buf, '\0', 512U); // gethostname() does not guarantee null-termination
        gethostname(hostname_buf, 511U);
        size_t len = strnlen(hostname_buf, 511) + 1;
        process_metric->value_handle = cmx_shm_add_value_string(cmx_shm_ptr, process_metric->name, (uint16_t) len);
        cmx_shm_set_value_string(cmx_shm_ptr, process_metric->value_handle, hostname_buf, (int) len);
    }
}

static void _metric_start_time(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    if (process_metric->value_handle == 0)
    {
        cmx_shm_value value = //
                { ._int64 = time(NULL) };
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static void _metric_rusage_utime(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    cmx_shm_value value;
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);

    if (process_metric->value_handle == 0)
    {
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
    }
    if (!(process_metric->value_handle < 0))
    {
        value._int64 = ((int64_t) usage.ru_utime.tv_sec * 1000L * 1000L) + (usage.ru_utime.tv_usec);
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static void _metric_rusage_stime(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    cmx_shm_value value;
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);

    if (process_metric->value_handle == 0)
    {
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
    }
    if (!(process_metric->value_handle < 0))
    {
        value._int64 = ((int64_t) usage.ru_stime.tv_sec * 1000L * 1000L) + (usage.ru_stime.tv_usec);
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static void _metric_rusage_nvcsw(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    cmx_shm_value value;
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);

    if (process_metric->value_handle == 0)
    {
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
    }
    if (!(process_metric->value_handle < 0))
    {
        value._int64 = usage.ru_nvcsw;
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static void _metric_rusage_nivcsw(cmx_shm * const cmx_shm_ptr, struct cmx_process_metric_t * const process_metric)
{
    cmx_shm_value value;
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);

    if (process_metric->value_handle == 0)
    {
        process_metric->value_handle = cmx_shm_add_value_single(cmx_shm_ptr, CMX_TYPE_INT64, process_metric->name);
    }
    if (!(process_metric->value_handle < 0))
    {
        value._int64 = usage.ru_nivcsw;
        cmx_shm_set_value_single(cmx_shm_ptr, process_metric->value_handle, CMX_TYPE_INT64, &value);
    }
}

static struct cmx_process_metric_t supported_metrics[] =
{ //
#ifdef MANIFEST_INFO
        { "manifest", _metric_manifest, 0 },
#endif
                { "process_name", _metric_process_name, 0 },
                { "process_id", _metric_process_id, 0 },
                { "hostname", _metric_hostname, 0 },
                { "start_time", _metric_start_time, 0},
                { "rusage.utime", _metric_rusage_utime, 0 },
                { "rusage.stime", _metric_rusage_stime, 0 },
                { "rusage.nvcsw", _metric_rusage_nvcsw, 0 },
                { "rusage.nivcsw", _metric_rusage_nivcsw, 0 },
                { NULL, NULL, 0 } };

static void _init(cmx_shm * cmx_shm_ptr)
{
    struct cmx_process_metric_t * metric = supported_metrics;
    while (metric->name != NULL && metric->function != NULL)
    {
        LOG_TRACE_IF("Add process metric: %s", metric->name);
        metric->value_handle = 0;
        metric->function(cmx_shm_ptr, metric);
        metric++;
    }
}

static void _update(cmx_shm * cmx_shm_ptr)
{
    struct cmx_process_metric_t * metric = supported_metrics;
    while (metric->name != NULL && metric->function != NULL)
    {
        if (metric->value_handle < 0) continue;
        LOG_TRACE_IF("Update process metric %s", metric->name);
        metric->function(cmx_shm_ptr, metric);
        metric++;
    }
}

void cmx_process_update(void)
{
    int current_once_pid = process_shm_once_pid;
    if (current_once_pid != cmx_common_current_pid())
    {
        if (cmx_atomic_val_compare_and_swap_int32(&process_shm_once_pid, current_once_pid, cmx_common_current_pid())
                == current_once_pid)
        {
            if (current_once_pid == 0)
            {
                LOG_INFO_IF("Initialise process component for process id=%d", cmx_common_current_pid());
            }
            else
            {
                LOG_INFO_IF("Process id changed from %d to %d. Re-initialise process component", current_once_pid,
                        cmx_common_current_pid());
            }
            cmx_registry_cleanup();
            cmx_shm * cmx_shm_ptr;
            if (cmx_shm_create("_", &cmx_shm_ptr) == E_CMX_SUCCESS)
            {
                _init(cmx_shm_ptr);
                process_shm = cmx_shm_ptr;
            }
            else
            {
                LOG_WARN_IF("Failed to create process component for pid=%d", cmx_common_current_pid());
                process_shm = NULL;
                // leave process_shm_once_pid with current pid. Otherwise every call to cmx_process_update would
                // try again to initialize the process component.
            }
        }
    }
    else
    {
        cmx_shm * cmx_shm_ptr = process_shm;
        // the cmx_shm_ptr must be checked because its possible that the initialization failed.
        if (cmx_shm_ptr != NULL) _update(cmx_shm_ptr);
    }
}
