/*
 * CMX (c) Copyright 2013 CERN
 * This software is distributed under the terms of the GNU Lesser General Public Licence version 3
 * (LGPL Version 3), copied verbatim in the file “COPYING”.
 * In applying this licence, CERN does not waive the privileges and immunities granted to it by virtue of its
 * status as an Intergovernmental Organization or submit itself to any jurisdiction. */
#define CMW_CMX_USE_PRIVATE
#include "cmw-cmx/atomic.h"

// == Generic

uint16_t cmx_atomic_read_uint16(volatile const uint16_t * ptr) __attribute__ ((noinline));
uint16_t cmx_atomic_read_uint16(volatile const uint16_t * ptr)
{
    return *ptr;
}

void cmx_atomic_set_uint16(volatile uint16_t * ptr, uint16_t val) __attribute__ ((noinline));
void cmx_atomic_set_uint16(volatile uint16_t * ptr, uint16_t val)
{
    *ptr = val;
}

int32_t cmx_atomic_read_int32(volatile const int32_t * ptr) __attribute__ ((noinline));
int32_t cmx_atomic_read_int32(volatile const int32_t * ptr)
{
    return *ptr;
}

void cmx_atomic_set_int32(volatile int32_t * ptr, int32_t val) __attribute__ ((noinline));
void cmx_atomic_set_int32(volatile int32_t * ptr, int32_t val)
{
    *ptr = val;
}

uint64_t cmx_atomic_read_uint64(volatile const uint64_t * ptr) __attribute__ ((noinline));
uint64_t cmx_atomic_read_uint64(volatile const uint64_t * ptr)
{
    return *ptr;
}

void cmx_atomic_set_uint64(volatile uint64_t * ptr, uint64_t val) __attribute__ ((noinline));
void cmx_atomic_set_uint64(volatile uint64_t * ptr, uint64_t val)
{
    *ptr = val;
}

// == Architecture specific implementation

#if defined(__x86_64__)
void cmx_atomic_smp_rmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_rmb(void)
{
    // lfence: Wait_On_Following_Instructions_Until(preceding_instructions_complete);
    __asm__ volatile("lfence" : : : "memory");
    return;
}

void cmx_atomic_smp_wmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_wmb(void)
{
    // sfence: Wait_On_Following_Stores_Until(preceding_stores_globally_visible);
    __asm__ volatile("sfence" : : : "memory");
    return;
}

void cmx_atomic_smp_mb(void) __attribute__ ((noinline));
void cmx_atomic_smp_mb(void)
{
    // mfence: Wait_On_Following_Loads_And_Stores_Until(preceding_loads_and_stores_globally_visible);
    __asm__ volatile("mfence" : : : "memory");
    return;
}
#elif defined(__i386__)
void cmx_atomic_smp_rmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_rmb(void)
{
    cmx_atomic_smp_mb();
    return;
}

void cmx_atomic_smp_wmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_wmb(void)
{
    cmx_atomic_smp_mb();
    return;
}

void cmx_atomic_smp_mb(void) __attribute__ ((noinline));
void cmx_atomic_smp_mb(void)
{
    __asm__ volatile("lock; addl $0,0(%%esp)" : : : "memory");
    return;
}
#elif __GNUC__ == 4 && __GNUC_MINOR__ >= 4
void cmx_atomic_smp_rmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_rmb(void)
{
    __sync_synchronize();
    return;
}

void cmx_atomic_smp_wmb(void) __attribute__ ((noinline));
void cmx_atomic_smp_wmb(void)
{
    __sync_synchronize();
    return;
}

void cmx_atomic_smp_mb(void) __attribute__ ((noinline));
void cmx_atomic_smp_mb(void)
{
    __sync_synchronize();
    return;
}
#else
#error "Unsupported platform for cmw-cmx/atomic.h"
#endif

// == Compiler and also architecture specific implementation
#if __GNUC__ == 4 && __GNUC_MINOR__ >= 4
// GCC >= 4.4: Implementation with Atomic __builtin's
// http://gcc.gnu.org/onlinedocs/gcc-4.4.0/gcc/Atomic-Builtins.html

const char * cmx_atomic_implementation_name(void)
{
    static const char * name = "gcc-builtin";
    return name;
}

int32_t cmx_atomic_val_compare_and_swap_int32(volatile int32_t * ptr, int32_t oldval, int32_t newval) __attribute__ ((noinline));
int32_t cmx_atomic_val_compare_and_swap_int32(volatile int32_t * ptr, int32_t oldval, int32_t newval)
{
    return __sync_val_compare_and_swap(ptr, oldval, newval);
}
#elif defined(__x86_64__) || defined(__i386__)
// Implementation with inline assembler for
// x86-32 and x86-64

const char * cmx_atomic_implementation_name(void)
{
    static const char * name = "ia32-assembler";
    return name;
}

int32_t cmx_atomic_val_compare_and_swap_int32(volatile int32_t * ptr, int32_t oldval, int32_t newval) __attribute__ ((noinline));
int32_t cmx_atomic_val_compare_and_swap_int32(volatile int32_t * ptr, int32_t oldval, int32_t newval)
{
    int32_t prev;
    __asm__ volatile ("lock; cmpxchgl %1, %2"
            : "=a" (prev)
            : "r" (newval), "m" (*(ptr)), "0"(oldval)
            : "memory", "cc");
    return prev;
}
#else
#error "Unsupported platform for cmw-cmx/atomic.h"
#endif

